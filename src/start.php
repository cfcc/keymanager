<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/6/16
 * Time: 11:30 AM
 */
$page_title = 'Check In / Check Out';

require_once "include/app_config.php";
require_once "include/database.php";
require_once "include/functions.php";

sec_session_start();

try {
    $config = load_config();
    $db = connect_key_db($config);
} catch (Exception $ex) {
    $db = null;
    error_log($ex);
    $fatal_error = $ex->getMessage();
}

$key_barcode = null;
$do_check_in = false;
$do_check_out = false;
$fail_station_required = false;

$station_required = getSetting('STATION_REQUIRED', $db, 'false');
$station_id = checkStation($db);

if (!is_null($station_id)) { $hide_logon_form = true; }

if ($station_required == 'true' and $station_id == null) {
    $fail_station_required = true;
} else {
    // Using REQUEST instead of POST so that I can redirect from the next step
    if (isset($_REQUEST['key_barcode'])) {
        // On the Raspberry Pi, this field is coming through as a blank, so we'll send the user back to the index page
        // with an error if that happens
        if (trim($_REQUEST['key_barcode']) == "") {
            $_SESSION['flash'] = "Please scan a key barcode";
            session_write_close();
            header('Location: index.php');
        }
        $key_barcode = $_REQUEST['key_barcode'];
        $stmt1 = $db->prepare("SELECT key_barcode FROM keyring
LEFT JOIN keyring_depts ON key_barcode=keyring_id
LEFT JOIN stations_depts ON keyring_depts.depts_id=stations_depts.depts_id
LEFT JOIN stations ON stations_depts.stations_id=stations.id
WHERE key_barcode=?
AND stations.id=?");
        $stmt1->execute(array($key_barcode, $station_id));
        $result = $stmt1->fetchAll(PDO::FETCH_ASSOC);
        // if the key barcode exists...
        if ($result) {
            $stmt2 = $db->prepare("SELECT * FROM checkout_log WHERE keyring_id=? AND in_timestamp IS NULL");
            $stmt2->execute(array($key_barcode));
            $result = $stmt2->fetchAll(PDO::FETCH_ASSOC);
            // If the key has not been checked out...
            if (!$result) {
                $page_title = "Check Out";
                $do_check_out = true;
            } else {
                // otherwise check the keys back in...
                $page_title = "Check In";
                $do_check_in = true;
                $stmt3 = $db->prepare('UPDATE checkout_log SET in_timestamp=NOW(),in_station=? WHERE id=?');
                $stmt3->execute(array($station_id, $result[0]['id']));
                $page_meta = array("<meta http-equiv=\"refresh\" content=\"3;url=index.php\" />");
            }
        }
    }
}
include_once 'include/header.php';
if ($do_check_out) {
    ?>
    <div class="row">
        <form action="checkout.php" method="post">
            <div class="form-group">
                <label for="key_barcode">Checking Out:</label>
                <input type="text" name="key_barcode" id="key_barcode" value="<?php echo $key_barcode; ?>" readonly>
            </div>
            <div class="form-group small-form-left">
                <label for="id_barcode" class="lead">ID Barcode</label>
                <input type="text" class="form-control" id="id_barcode" name="id_barcode" autofocus autocomplete="off"
                       required>
                <p class="help-block" class="lead">Scan your ID Card barcode or enter your Colleague ID</p>
            </div>
            <div class="btn-group">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href='index.php' class='btn btn-default'>Back</a>
            </div>
        </form>
    </div>
    <?php
} elseif ($do_check_in) {
    ?>
    <h2 class="text-success">Keys have been Checked In</h2>
    <p>The key ring, <?php echo $key_barcode; ?>, has been checked back in.</p>
    <p>Thank you!</p>
    <script type="text/JavaScript">
        setTimeout("location.href = 'index.php';", 2500);
    </script>
    <?php
} elseif ($fail_station_required) {
    echo "<div class='alert alert-danger'>\n";
    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>\n";
    echo "<span class=\"sr-only\">Error:</span>\n";
    echo "This computer is not a valid check-out station!</div>\n";
    echo "<p><a href='index.php' class='btn btn-default'>Back</a></p>\n";
} else {
    echo "<div class='alert alert-danger'>\n";
    echo "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>\n";
    echo "<span class=\"sr-only\">Error:</span>\n";
    echo "This is not a valid key barcode for this station: " . $key_barcode . "</div>\n";
    echo "<p><a href='index.php' class='btn btn-default'>Back</a></p>\n";
}
require_once "include/footer.php";
