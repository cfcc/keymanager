<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/27/16
 * Time: 9:45 AM
 */
$page_title = "Admin:Station";
$include_path = "../";
$logout_url = "../index.php";
$js_include = array(
    'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',
    '../resources/js/dialogs.js'
);
$page_css = array(
    'https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
);
$page_script = array(
    "$(document).ready(function() {",
    "  $('#settings_list').DataTable( { searching: false } );",
    "} );"
);

require_once "../include/app_config.php";
require_once '../include/database.php';
require_once "../include/functions.php";

$config = load_config();
$db = connect_key_db($config);
$ods_db = connect_ods_db($config);

sec_session_start();

require_once "../include/header.php";

$station_id = checkStation($db);

# $uid = md5($_SERVER['HTTP_USER_AGENT'] .  $_SERVER['REMOTE_ADDR']);
$uid = md5($_SERVER['REMOTE_ADDR']);

if (login_check($config, $db, ACCESS_MANAGER) == true) {
    if (array_key_exists('station_id', $_REQUEST)) {
        $access_allowed = false;
        $query = "SELECT * FROM stations LEFT JOIN stations_depts
                        ON stations_depts.stations_id=stations.id
                        WHERE id=?";
        $stmt = $db->prepare($query);
        $stmt->execute(array($_REQUEST['station_id']));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($result) {
            $record = $result[0];
            $my_depts = getDeptsForStation($_REQUEST['station_id'], $db, $ods_db);
            if ($record['ip_address']) {
                $my_ip_address = inet_ntop($record['ip_address']);
            } else {
                $my_ip_address = "";
            }
            if ($_SESSION['access'] == ACCESS_ADMINISTRATOR) {
                # by default Administrators have access to everything
                $access_allowed = true;
            } else {
                foreach ($_SESSION['user_info']['groups'] as $user_dept) {
                    if (in_array($user_dept, array_keys($my_depts))) {
                        $access_allowed = true;
                        break;
                    }
                }
            }
        } else {
            $access_allowed = true;
            $my_ip_address = $_SERVER['REMOTE_ADDR'];
            $record = array(
                'id' => '',
                'uid' => $uid,
                'name' => '',
                'is_active' => 0,
            );
            $my_depts = array();
        }
        if ($access_allowed) {
            $all_depts = getAllDepts($ods_db, $db);
            if ($_REQUEST['action'] == 'edit') {
                $station_required = getSetting('STATION_REQUIRED', $db, 'false');

                echo "<p>Use this form to set up the computer as a check-out station.</p>\n";
                echo "<p>Currently a station is ";
                if ($station_required != "true") {
                    echo "<em>not</em> ";
                }
                echo "required to check out a key.</p>\n";

                ?>
                <div class="row">
                    <div class="col-md-6 col-md-offset-1">
                <form method="post">
                    <input type="hidden" name="action" value="save">
                    <input type="hidden" name="current_id" value="<?php echo $record['id'] ?>" class="form-control">
                    <div class="form-group">
                        <label for="station_id">Station ID</label>
                        <input type="text" name="station_id" id="station_id" value="<?= $record['id'] ?>" readonly class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="ip_address">IP Address</label>
                        <input type="text" name="ip_address" id="ip_address" value="<?= $my_ip_address ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="station_uid">Unique ID</label>
                        <input type="text" name="station_uid" id="station_uid" value="<?= $record['uid'] ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="station_name">Station Name</label>
                        <input type="text" name="station_name" id="station_name" value="<?= $record['name'] ?>" class="form-control" autofocus>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="is_active" name="is_active"
                                    <?php if ($record['is_active']) {
                                        echo ' checked';
                                    } ?>> Station Active
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <p>Departments associated with this station:</p>
                        <ul>
                        <?php
                        foreach ($my_depts as $dept_code => $dept_name) {
                            echo "<div class='checkbox'><li>\n";
                            echo "<strong>" . $dept_name . "</strong>\n";
                            echo "( <label>\n";
                            echo "<input type='checkbox' id='del_dept[" . $dept_code . "]' name='del_dept[" . $dept_code . "]'>";
                            echo "Delete\n";
                            echo "</label> )\n";
                            echo "</li></div>\n";
                        }
                        echo "</ul></div>\n";
                        echo "<div class='form-group'>\n";
                        echo "<label for='add_dept'>Add Deptartment</label>\n";
                        echo "<select id='add_dept' name='add_dept'>\n";
                        echo "<option value='0'>Select a department...</option>\n";
                        foreach ($all_depts as $dept_code => $dept_name) {
                            echo "<option value='" . $dept_code . "'>" . $dept_name . "</option>\n";
                        }
                        echo "</select>\n";
                        echo "</div>\n";
                        ?>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="station.php" class="btn btn-default">Cancel</a>
                            <?php
                            if (!empty($record['id'])) {
                                echo "<a href=\"station.php?station_id=" . $record['id'] . "&amp;action=set\" class=\"btn btn-default\">Set as Station</a>";
                                echo "<a href=\"station.php?station_id=" . $record['id'] . "&amp;action=remove\" class=\"btn btn-default\">Remove Station</a>";
                            }
                            ?>
                        </div>
                </form>
                    </div>
                </div>
                <?php
            } elseif ($_REQUEST['action'] == 'delete') {
                $stmt_del = $db->prepare('DELETE FROM stations WHERE id=?');
                try {
                    $stmt_del->execute(array($_REQUEST['station_id']));
                    echo "<p>Successfully deleted station " . $_REQUEST['station_id'] . "</p>\n";
                } catch (Exception $e) {
                    echo "<p>Error deleting station</p>\n";
                    echo "<p>" . $e->getMessage() . "</p>\n";
                }
                echo "<div class='btn-group'>";
                echo "<a href='station.php' class='btn btn-default'>Continue</a>";
                echo "</div>\n";
            } elseif ($_REQUEST['action'] == 'save') {
                $records_changed = 0;
                $station_id = null;
                $station_uid = $_POST['station_uid'];
                if (isset($_REQUEST['is_active'])) {
                    $is_active = 1;
                } else {
                    $is_active = 0;
                }
                if (isset($_REQUEST['ip_address'])) {
                    $ip_bin = inet_pton($_REQUEST['ip_address']);
                    # FIXME: is this the best way to update the station UID?
                    $new_station_uid = md5($_REQUEST['ip_address']);
                    if ($new_station_uid != $station_uid) {
                        $station_uid = $new_station_uid;
                    }
                } else {
                    $ip_bin = NULL;
                }
                if (!empty($_POST['current_id'])) {
                    $stmt = $db->prepare("UPDATE stations SET uid=?, name=?, is_active=?, ip_address=? WHERE id=?");
                    $stmt->execute(array($_POST['station_uid'], $_POST['station_name'], $is_active, $ip_bin, $_POST['current_id']));
                    $records_changed = $stmt->rowCount();
                    $station_id = $_POST['current_id'];
                } else {
                    $stmt = $db->prepare("INSERT INTO stations (uid, name, ip_address, is_active) VALUES (?, ?, ?, ?)");
                    $stmt->execute(array($_POST['station_uid'], $_POST['station_name'], $ip_bin, $is_active));
                    $records_changed = $stmt->rowCount();
                    $station_id = $db->lastInsertId();
                }
                if ($station_id != null) {
                    if (isset($_REQUEST['del_dept'])) {
                        $records_removed = 0;
                        $stmt = $db->prepare("DELETE FROM stations_depts WHERE stations_id=? AND depts_id=?");
                        foreach ($_REQUEST['del_dept'] as $key => $value) {
                            $stmt->execute(array($station_id, $key));
                            $records_removed += $stmt->rowCount();
                        }
                        echo "<p>Removed " . $records_removed . " record(s)</p>";
                    }
                    if ($_REQUEST['add_dept'] != '0') {
                        $stmt = $db->prepare('INSERT INTO stations_depts (stations_id, depts_id) VALUES (?, ?)');
                        $stmt->execute(array($station_id, $_REQUEST['add_dept']));
                        $records_changed += $stmt->rowCount();
                    }
                } else {
                    echo "<p>Station ID = " . $station_id . ", add_dept = " . $_REQUEST['add_dept'] . "</p>\n";
                }
                echo "<p>Updated records: " . $records_changed . "</p>\n";
                echo "<p><a href='station.php' class='btn btn-default'>Continue</a></p>";

            } elseif ($_REQUEST['action'] == 'set') {
                if ($_REQUEST['station_id']) {
                    $station_id = $_REQUEST['station_id'];
                    $expiration = time() + 3600 * 48;
                    setcookie('keymanager[station_id]', $station_id, $expiration, '/');
                    echo "<div class='alert alert-success'>\n";
                    echo "Set cookie to " . $station_id . "\n";
                    echo "</div>\n";
                    echo "<p><a href='station.php' class='btn btn-default'>Continue</a></p>\n";
                }
            } elseif ($_REQUEST['action'] == 'remove') {
                setcookie('keymanager[station_id]', '', time() - 3600, '/');
                echo "<div class='alert alert-success'>\n";
                echo "Removed cookie marking this computer as a station\n";
                echo "</div>\n";
                echo "<p><a href='station.php' class='btn btn-default'>Continue</a></p>\n";
            }
        } else {
            echo "<h2>Access Denied</h2>\n";
            echo "<p>You don't have access to modify that station.</p>\n";
            echo "<p><a href=\"station.php\" class=\"btn btn-default\">Back</a></p>\n";
            # echo "<pre>" . print_r(getDeptsForStation($_REQUEST['station_id'], $db, $ods_db), true) . "</pre>\n";
        }
    } else {
        // display the table here
        ?>
        <div class="col-md-2 col-md-offset-10">
            <p><a href="station.php?action=edit&station_id=" class="btn btn-default">Add</a></p>
        </div>
        <table id="station_list" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>UID</th>
                <th>IP</th>
                <th>Name</th>
                <th>Departments</th>
                <th>Active?</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $stmt_dept_list = $db->prepare("SELECT depts_id from stations_depts WHERE stations_id=?");

            $stmt = $db->query("SELECT * FROM stations ORDER BY name");
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $stmt_dept_list->execute(array($row['id']));
                $this_depts = array();
                foreach ($stmt_dept_list->fetchAll(PDO::FETCH_ASSOC) as $dept_row) {
                    if ($dept_row) {
                        $this_depts[] = $dept_row['depts_id'];
                    }
                }
                if ($row['ip_address']) {
                    $ip_address = inet_ntop($row['ip_address']);
                } else {
                    $ip_address = "";
                }
                ?>
                <tr>
                    <td><?php echo $row['id']; ?></td>
                    <td><?php echo $row['uid']; ?></td>
                    <td><?php echo $ip_address; ?></td>
                    <td><?php echo $row['name']; ?></td>
                    <td><?php echo join(", ", $this_depts); ?></td>
                    <td><?php echo $row['is_active']; ?></td>
                    <td>
                        <div class="btn-group">
                            <a href="station.php?action=edit&amp;station_id=<?php echo $row['id'] ?>" class="btn btn-default">Edit</a>
                            <a href="station.php?action=delete&amp;station_id=<?php echo $row['id'] ?>" class="btn btn-default" onclick="return confirm('Delete: are you sure?'); ">Delete</a>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <?php
    }
} else {
    echo "<p>You are not authorized to access this page.  Please login first.</p>";
}

require_once("../include/footer.php");
