<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 9/27/16
 * Time: 1:50 PM
 *
 * NOTE: To set this up in CRON use the script etc/notify.sh
 *
 */

require_once "../include/app_config.php";
require_once '../include/database.php';
require_once "../include/functions.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require '../vendor/autoload.php';

try {
    $config = load_config();
    $db = connect_key_db($config);
    $ods_db = connect_ods_db($config);
} catch (\Exception $e) {
    echo "Failed to load the configuration file: " . $e->getMessage();
    exit(1);
}

$FROM_ADDR = "From: Key Checkout <alerts@checkout.ad.cfcc.edu>";
$FROM_ADDR = "From: Key Checkout <no-reply@cfcc.edu>";

$stmt_keys = $db->prepare("SELECT id, keyring_id, colleague_id, last_name, first_name, supervisor_email, out_timestamp, alerts_sent, TIMESTAMPDIFF(HOUR, out_timestamp, CURRENT_TIMESTAMP()) as hours_out
 FROM checkout_log 
 LEFT JOIN staff ON checkout_log.staff_id = staff.id_barcode
 WHERE in_timestamp IS NULL 
 AND TIMESTAMPDIFF(HOUR, out_timestamp, CURRENT_TIMESTAMP()) > ?");

$stmt_hist = $db->prepare("SELECT id, keyring_id, colleague_id, last_name, first_name, supervisor_email, out_timestamp, in_timestamp, alerts_sent, TIMESTAMPDIFF(HOUR, out_timestamp, CURRENT_TIMESTAMP()) as hours_out
 FROM checkout_log
 LEFT JOIN staff ON checkout_log.staff_id = staff.id_barcode
 WHERE alerts_sent > 0");

$stmt_update_log = $db->prepare("UPDATE checkout_log SET alerts_sent=? WHERE id=?");

$stmt_name = $ods_db->prepare("SELECT ID, FIRST_NAME, LAST_NAME, PERSON_EMAIL_ADDRESSES FROM CFCC_PERSON_EMAIL_VIEW WHERE ID = ? AND PERSON_EMAIL_TYPES='EE'");

$max_time_out = getSetting("MAX_HOURS_OUT", $db);

if (php_sapi_name() == "cli") {
    // The script has been launched from CRON, and so we send out the email if any records are returned by the query.
    $options = getopt("dfhv");

    if (array_key_exists('h', $options)) {
        echo "Usage: notify.php [-v][-f]\n\nUse -v for verbose output.\nUse -f to force sending an email\n";
        exit(0);
    }

    if (array_key_exists('v', $options)) { $verbose = true; } else { $verbose = false; }

    if (array_key_exists('f', $options)) { $force_send = true; } else { $force_send = false; }

    if (array_key_exists('d', $options)) { $debug_mode = true; } else { $debug_mode = false; }

    $stmt_keys->execute(array($max_time_out));
    $result = $stmt_keys->fetchAll(PDO::FETCH_ASSOC);

    $cnt = 0;
    foreach ($result as $row) {
        // Each time we find an overdue key check to see how many alerts we've sent and increase the delay by 4 for
        // every time we sent an alert, so an email is sent on the hours: 0, 1, 4, 8, 12, 16 ...
        $can_send = $row['alerts_sent'] <= 0 || ($row['hours_out'] - $max_time_out) % ($row['alerts_sent'] * 4) == 0;
        if($can_send || $force_send) {
            if ($row['colleague_id']) {
                $stmt_name->execute(array($row['colleague_id']));
                $ods_result = $stmt_name->fetchAll(PDO::FETCH_ASSOC);
                $employee_name = $ods_result[0]['FIRST_NAME'] . " " . $ods_result[0]['LAST_NAME'];
                $employee_last = $ods_result[0]['LAST_NAME'];
                $employee_email = $ods_result[0]['PERSON_EMAIL_ADDRESSES'];
            } else {
                $employee_name = $row['first_name'] . ' ' . $row['last_name'];
                $employee_last = $row['last_name'];
                $employee_email = null;
            }
            // Also email the supervisor if that option is turned on in the settings file
            if ($config['mail']['supervisor_cc']) {
                if ($row['colleague_id']) {
                    $supervisor_email = getSupervisorEmail($ods_db, $row['colleague_id']);
                } else {
                    $supervisor_email = $row['supervisor_email'];
                }
            } else {
                $supervisor_email = null;
            }
            if (!$debug_mode) {
                $mail = new PHPMailer(true);
                $mail->CharSet = 'UTF-8';

                $mail->isSMTP();
                $mail->Host = $config['mail']['host'];

                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                $mail->Port = $config['mail']['port'];
                $mail->SMTPAuth = true;

                $mail->Username = $config['mail']['username'];
                $mail->Password = $config['mail']['password'];

                // Send a reminder to the employee, but only if we could get the email from Colleague
                if ($employee_email) {
                    $message = "Hello " . $employee_name . ",\r\n\r\n";
                    $message .= "The keyring " . $row['keyring_id'] . " has been checked out longer than allowed.  Please return it as soon as possible.\r\n\r\n";
                    $message .= "The keyring was checked out on " . $row['out_timestamp'] . "\r\n\r\n";

                    $message = wordwrap($message, 70, "\r\n");

                    $subject = "Key Notice: " . $row['keyring_id'];

                    try {
                        $mail->SetFrom('no-reply@cfcc.edu', 'no-reply');
                        $mail->addAddress($employee_email, $employee_name);
                        if ($supervisor_email) {
                            $mail->addCC($supervisor_email);
                        }
                        $mail->Subject = $subject;
                        $mail->Body = $message;
                        $mail->send();
                        $stmt_update_log->execute(array($row['alerts_sent'] + 1, $row['id']));
                        $cnt++;
                        error_log("Message successfully sent to {$employee_email}");
                    } catch (Exception $e) {
                        error_log("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
                    }
                } else {
                    error_log("ERROR: employee email is blank for employee ID: {$row['colleague_id']}");
                }
            } else {
                echo "[DEBUG] Send notice for " . $row['keyring_id'] . " checked out by " . $employee_name . "(" . $row['colleague_id'] . ") on " . $row['out_timestamp'] . "\n";
            }
        } else {
            if ($verbose) {
                if ($debug_mode) {
                    echo "[DEBUG] Skipping " . $row['keyring_id'] . " checked out by " . $row['colleague_id'] . " on " . $row['out_timestamp'] . "\n";
                } else {
                    echo "Skipping alert because " . $row['alerts_sent'] . ", next alert in " . ($row['alerts_sent'] * 4) . " hours.\n";
                }
            }
        }
    }
    if ($verbose) {
        echo "Complete.  Sent " . $cnt . " email(s) this run.\n";
    }
} else {
    // Not in cli-mode
    $page_title = "Admin:Notify";
    $include_path = "../";
    $logout_url = "../index.php";
    $js_include = array(
        'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'
    );
    $page_css = array(
        'https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
    );
    $page_script = array(
        "$(document).ready(function() {",
        "  $('#notify_list').DataTable( {",
        "      \"searching\": false,",
        "      \"order\": [[ 4, \"desc\" ]]",
        " } );",
        "} );"
    );

    sec_session_start();

    require_once "../include/header.php";

    if (login_check($config, $db, ACCESS_MANAGER)) {
        if (isset($_REQUEST['history'])) {
            $stmt_hist->execute();
            $result = $stmt_hist->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $stmt_keys->execute(array($max_time_out));
            $result = $stmt_keys->fetchAll(PDO::FETCH_ASSOC);
        }
        echo "<table id='notify_list' class='display'>\n";
        echo "<thead>\n";
        echo "<tr>\n";
        echo "<th>Keyring</th>\n";
        echo "<th>Staff Id</th>\n";
        echo "<th>Staff Name</th>\n";
        echo "<th>Staff Email</th>\n";
        echo "<th>Time Out</th>\n";
        echo "<th>Hours Out</th>\n";
        echo "<th>Alerts Sent</th>\n";
        echo "<th>Supervisor Email</th>\n";
        echo "</tr>\n";
        echo "</thead>\n";
        echo "<tbody>\n";
        foreach ($result as $row) {
            if ($row['colleague_id']) {
                $supervisor_email = getSupervisorEmail($ods_db, $row['colleague_id']);

                $stmt_name->execute(array($row['colleague_id']));
                $ods_result = $stmt_name->fetchAll(PDO::FETCH_ASSOC);

                if ($ods_result) {
                    $employee_name = $ods_result[0]['FIRST_NAME'] . " " . $ods_result[0]['LAST_NAME'];
                    $employee_email = $ods_result[0]['PERSON_EMAIL_ADDRESSES'];
                } else {
                    $employee_name = "";
                    $employee_email = "";
                }
            } else {
                $supervisor_email = $row['supervisor_email'];
                $employee_name = $row['first_name'] . ' ' . $row['last_name'];
                $employee_email = "";
            }
            if (array_key_exists('in_timestamp', $row) && $row['in_timestamp'] != null) {
                $ts_out = new DateTime($row['out_timestamp']);
                $ts_in = new DateTime($row['in_timestamp']);
                $ts_diff = $ts_in->diff($ts_out);
                $hours_out = $ts_diff->h + ($ts_diff->days * 24);
            } else {
                $hours_out = $row['hours_out'];
            }
            echo "<tr>\n";
            echo "<td>" . $row['keyring_id'] . "</td>\n";
            echo "<td>" . $row['colleague_id'] . "</td>\n";
            echo "<td>" . $employee_name . "</td>\n";
            echo "<td>" . $employee_email . "</td>\n";
            echo "<td>" . format_timestamp($row['out_timestamp']) . "</td>\n";
            echo "<td>" . $hours_out . "</td>\n";
            echo "<td>" . $row['alerts_sent'] . "</td>\n";
            echo "<td>" . $supervisor_email . "</td>\n";
            echo "</tr>\n";
        }
        echo "</tbody>\n";
        echo "</table>\n";
?>
<script type="application/javascript">
    let table = new DataTable('#notify_list', {
        "searching": false,
        "order": [[ 4, "desc" ]]
        });
</script>
<?php
    } else {
        echo "<p>You are not authorized to access this page.  Please login first.</p>";
    }
    require_once '../include/footer.php';
}
