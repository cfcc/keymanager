<?php
/**
 * Created by PhpStorm.
 * User: jfriant
 * Date: 7/10/16
 * Time: 2:47 PM
 */
require_once "../vendor/cfcc/cfcc-ad-authentication-module/AuthenticationModuleClass.php";
require_once "functions.php";
require_once "app_config.php";

$config = load_config();

sec_session_start();

try {
    $authenticator = new AuthenticationModule('/home/keymanager/authentication_module.ini');
    $authentication_result = $authenticator->authenticate($_POST['username'], $_POST['password']);
    if ($authentication_result) {
            $_SESSION['user_info'] = $authenticator->get_user_info();
            $_SESSION['access'] = authorize($_SESSION['user_info']['groups'], $config);

            header('HTTP/1.1 200 Authentication: Success');
    } else {
            header('HTTP/1.1 401 Authentication: Failure');
    }
} catch (Exception $e) {
    header('HTTP/1.1 500 ' . "Caught Exception:" . $e->getMessage());
}
exit;
