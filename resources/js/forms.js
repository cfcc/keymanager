/*
 * Form functions.
 *
 * Downloaded from: http://www.wikihow.com/Create-a-Secure-Login-Script-in-PHP-and-MySQL
 */
function formhash(form, password) {
    // Create a new element input, this will be our hashed password field.
    var p = document.createElement("input");

    // Add the new element to our form.
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);

    // Make sure the plaintext password doesn't get sent.
    password.value = "";

    // Finally submit the form.
    form.submit();
}

function regformhash(form, uid, email, password, conf) {
    // Check each field has a value
    if (uid.value == ''         ||
        email.value == ''     ||
        password.value == ''  ||
        conf.value == '') {

        alert('You must provide all the requested details. Please try again');
        return false;
    }

    // Check the username

    re = /^\w+$/;
    if(!re.test(form.username.value)) {
        alert("Username must contain only letters, numbers and underscores. Please try again");
        form.username.focus();
        return false;
    }

    // Check that the password is sufficiently long (min 6 chars)
    // The check is duplicated below, but this is included to give more
    // specific guidance to the user
    if (password.value.length < 6) {
        alert('Passwords must be at least 6 characters long.  Please try again');
        form.password.focus();
        return false;
    }

    // At least one number, one lowercase and one uppercase letter
    // At least six characters

    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    if (!re.test(password.value)) {
        alert('Passwords must contain at least one number, one lowercase and one uppercase letter.  Please try again');
        return false;
    }

    // Check password and confirmation are the same
    if (password.value != conf.value) {
        alert('Your password and confirmation do not match. Please try again');
        form.password.focus();
        return false;
    }

    // Create a new element input, this will be our hashed password field.
    var p = document.createElement("input");

    // Add the new element to our form.
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden";
    p.value = hex_sha512(password.value);

    // Make sure the plaintext password doesn't get sent.
    password.value = "";
    conf.value = "";

    // Finally submit the form.
    form.submit();
    return true;
}

function toggleNameFields(val) {
    var fields = [document.getElementById('first_name'), document.getElementById('last_name'), document.getElementById('dept_code'), document.getElementById('supervisor_email')];
    if (val) {
        if (val.length === 7) {
            $.ajax({
                url: '../data/ods/staff.php',
                type: 'get',
                data: {'id': val },
                success: function(data, status) {
                    if(data) {
                        fields[0].value = data['first_name'];
                        fields[1].value = data['last_name'];
                        fields[2].value = data['dept_code'];
                        fields[3].value = "";
                            }
                }
            })
        }
        for (i=0; i<fields.length; i++) {
            fields[i].disabled = true;
        }
    } else {
        for (i=0; i<fields.length; i++) {
            fields[i].disabled = false;
        }
    }
}

// FIXME: This should load the id_barcode into the form field on the start.php page.
$(function(){
    $('#altloginbtn').on('click'), function(e){
        e.preventDefault();
        $('#altloginbtn').fadeOut(300);

        $.ajax({
            url: 'ldapcheck.php',
            type: 'post',
            data: {'username': 'something', 'password': 'something'},
            success: function(data, status) {
                if(data) {}
            }
        })
    }
});
