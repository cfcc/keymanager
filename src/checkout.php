<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/6/16
 * Time: 11:47 AM
 */
require_once "include/app_config.php";
require_once "include/database.php";
require_once "include/functions.php";

try{
    $config = load_config();
    $db = connect_key_db($config);
} catch (Exception $ex) {
    $db = null;
    error_log($ex);
    $fatal_error = $ex->getMessage();
}

sec_session_start();

$station_id = checkStation($db);

if (!is_null($station_id)) { $hide_logon_form = true; }

$page_meta = array("<meta http-equiv=\"refresh\" content=\"5;url=index.php\" />");
$page_title = 'Check Out';

require_once "include/header.php";

if (isset($_POST['id_barcode']) && isset($_POST['key_barcode'])) {
    $key_barcode = $_POST['key_barcode'];
    if (trim($_POST['id_barcode']) == "") {
        $_SESSION['flash'] = 'Please scan or type an ID';
        session_write_close();
        header("Location: start.php?key_barcode=".$key_barcode);
    }
    if (is_numeric($_POST['id_barcode'])) {
        $id_barcode = $_POST['id_barcode'];
    } else {
        $id_barcode = null;
    }

    if (strlen($_POST['id_barcode']) == 7) {
        $stmt = $db->prepare('SELECT id_barcode, colleague_id FROM staff WHERE colleague_id=? AND is_active=TRUE');
    } else {
        $stmt = $db->prepare('SELECT id_barcode, colleague_id FROM staff WHERE id_barcode=? AND is_active=TRUE');
    }
    $stmt->execute(array($id_barcode));
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    # FIXME: This is where we would verify that the employee has the correct department if we want to limit check-out
    if ($result) {
        $id_barcode = $result[0]['id_barcode'];
        $colleague_id = $result[0]['colleague_id'];

        $stmt1 = $db->prepare("INSERT INTO checkout_log (out_timestamp, keyring_id, staff_id, out_station) VALUES (NOW(), ?, ?, ?)");
        $stmt1->execute(array($key_barcode, $id_barcode, $station_id));
        $affected_rows = $stmt1->rowCount();

        try {
            $dt_utc = new DateTimeImmutable('now', new DateTimeZone('UTC'));
            $dt_local = $dt_utc->setTimezone(new DateTimeZone('America/New_York'));
            $checkout_time = $dt_local->format("h:i:sa");
        } catch (DateMalformedStringException $e) {
            error_log($e->getMessage());
            $checkout_time = "ERROR";
        }

        echo "<h2 class='text-success'>Keys have been Checked Out</h2>\n";
        echo "<pre class='small-form-left'>\n";
        echo "Key Ring: " . $key_barcode . "\n";
        echo "ID Card: " . $id_barcode . "\n";

        echo "Check-out Time: " . $checkout_time . "\n";
        echo "</pre>\n";
        echo "";
        echo "<p>Updated rows: " . $affected_rows . "</p>";
    } else {
        ?>
        <div class="alert alert-danger" role="alert">
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            <span class="sr-only">Error:</span>
            The ID Barcode: <?= $_POST['id_barcode'] ?> is not authorized to check out keys.
        </div>
        <p><a href='index.php' class='btn btn-default'>Start Over</a></p>
       <?php
    }
} else {
    ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        Invalid parameters, please start over...
    </div>
    <p><a href='index.php' class='btn btn-default'>Start Over</a></p>
    <?php
}

require_once "include/footer.php";
