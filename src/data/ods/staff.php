<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 10/6/17
 * Time: 10:48 AM
 */
header("content-type:application/json");

include_once '../../include/app_config.php';
include_once '../../include/database.php';
include_once '../../include/functions.php';

$config = load_config();
$ods_db = connect_ods_db($config);


$json_out = array(
    'first_name' => null,
    'last_name' => null,
    'dept_code' => null
);

if (isset($_REQUEST['id'])) {
    $record = getStaffInfo($ods_db, $_REQUEST['id']);
    $json_out['first_name'] = $record['FIRST_NAME'];
    $json_out['last_name'] = $record['LAST_NAME'];
    $json_out['dept_code'] = $record['HRP_PRI_DEPT_SORT'];
} else {
    header('HTTP/1.1 500 ' . "Missing required parameter: id");
}

echo json_encode($json_out);

exit();
