<?php
/**
 * Created by PhpStorm.
 * User: jfriant
 * Date: 7/10/16
 * Time: 2:43 PM
 */

function connect_key_db($config): ?PDO
{
    $options = array(
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );

    try {
        $dsn = "mysql:host=" . $config['database']['host'] . ";dbname=" . $config['database']['dbname'] . ";charset=" . $config['database']['charset'];
        $db = new PDO($dsn, $config['database']['username'], $config['database']['password'], $options);
    } catch (Exception $e) {
        $db = NULL;
        $fatal_error = print_r($e->getMessage(), true) . " for MySQL Database";
        error_log($fatal_error);
    }
    return $db;
}

function connect_ods_db($config): ?PDO
{
    try {
        $dsn = "sqlsrv:server=".$config['ods']['host'].";Database = ".$config['ods']['dbname'].";Encrypt=true;TrustServerCertificate=true";
        $ods_db = new PDO($dsn, $config['ods']['username'], $config['ods']['password']);
        $ods_db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    } catch (Exception $e) {
        $ods_db = NULL;
        $fatal_error = print_r($e->getMessage(), true) . " for MS SQL Database";
        error_log($fatal_error);
    }
    return $ods_db;
}
