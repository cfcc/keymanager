<?php
/**
 * Created by PhpStorm.
 * User: jfriant
 * Date: 9/21/17
 * Time: 12:26 PM
 */

$page_title = "Admin:Barcode Labels";
$include_path = "../";
$logout_url = "../index.php";
$page_meta = array("<link href='https://fonts.googleapis.com/css?family=Pragati Narrow' rel='stylesheet'>",
    "<link rel='stylesheet' href='../resources/css/barcode-label-print.css' media='print'>"
);

require_once "../include/app_config.php";
require_once '../include/database.php';
require_once "../include/functions.php";

$config = load_config();
$db = connect_key_db($config);
$ods_db = connect_ods_db($config);

sec_session_start();

require_once "../include/header.php";

if (login_check($config, $db, ACCESS_USER)) {
    if ($_SESSION['access'] == ACCESS_ADMINISTRATOR) {
        $stmt = $db->query("SELECT key_barcode, description FROM keyring");
    } else {
        $allowed_depts = $_SESSION['user_info']['groups'];
        $questionmarks = str_repeat("?,", count($allowed_depts)-1) . "?";
        $stmt = $db->prepare("SELECT key_barcode, description
                                        FROM keyring
                                        LEFT JOIN keyring_depts ON keyring.key_barcode = keyring_depts.keyring_id
                                        WHERE depts_id IN ($questionmarks)");
        $stmt->execute($allowed_depts);
    }
    ?>
    <!--
    This is set for 2/3 in x 1 3/4 in labels that match the Avery 8195 and 5195 templates
    The grid is 4 x 15.
    -->
    <div class="barcode-table">
        <table class="table-bordered">
            <tr>
        <?php
        $cnt = 0;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $cnt++;
            $code_type = 'code128b';
            $code_size = 20;
            $print_text = 'true';
            $img_url = "../include/php-barcode/barcode.php?text=" . $row['key_barcode'] . "&codetype=" . $code_type . "&size=" . $code_size . "&print=" . $print_text;
            $max_length = 50;
            if (strlen($row['description']) > $max_length) {
                $key_desc = trim(substr($row['description'], 0, $max_length)) . "&hellip;";
            } else {
                $key_desc = $row['description'];
            }
            ?>
                <td class="barcode-box">
                    <!-- <div class="barcode-description"><?= $key_desc ?></div> -->
                    <img alt="<?= $key_desc ?>" src="<?= $img_url ?>">
                </td>
                <?php
            if ($cnt % 4 == 0) {
                echo "</tr>\n<tr>\n";
            }
            if ($cnt % 60 == 0) {
                echo "</table>\n</div>\n<div class='barcode-table page-break'>\n<table class='table-bordered'>\n<tr>";
            }
        }
        ?>
        </table>
    </div>
    <?php
} else {
    echo "<p>You are not authorized to access this page.  Please login first.</p>";
}
require_once "../include/footer.php";
