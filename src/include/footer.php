<?php
    /**
     * Created by PhpStorm.
     * User: jfriant80
     * Date: 7/7/16
     * Time: 9:32 AM
     */
    if (empty($include_path)) { $include_path = ""; }
    echo "</div>\n"; // .container

    if (isset($DEBUG) && $DEBUG == true) {
        echo "<div class='debug'>\n";
        echo "<div class='container'>";
        echo "<h2>DEBUG Information</h2>\n";
        echo "<pre>Session info: " . print_r($_SESSION, true) . "</pre>\n";
        echo "</div>\n";
        echo "</div>\n";
    }

    echo "<!-- Include all compiled plugins (below), or include individual files as needed -->\n";
    echo "<script src=\"". $include_path . "resources/js/bootstrap.min.js\"></script>\n";
    echo "</body>\n";
    echo "</html>\n";
