<?php
/**
 * Created by PhpStorm.
 * User: jfriant
 * Date: 7/26/16
 * Time: 7:06 PM
 */

$page_title = "Admin:Barcodes";
$include_path = "../";
$logout_url = "../index.php";
$page_meta = array("<link href='https://fonts.googleapis.com/css?family=Pragati Narrow' rel='stylesheet'>",
    "<link rel='stylesheet' href='../resources/css/barcode-print.css' media='print'>"
);

require_once "../include/app_config.php";
require_once '../include/database.php';
require_once "../include/functions.php";

$config = load_config();
$db = connect_key_db($config);

sec_session_start();

require_once "../include/header.php";

if (login_check($config, $db, ACCESS_USER)) {
    if (!empty($_REQUEST['pagenum'])) {
        $pagenum = intval($_REQUEST['pagenum']);
        if ($pagenum < 1) {
            $pagenum = 1;
        }
    } else {
        $pagenum = 1;
    }
    # calculate the start and ending records to show, for example:
    # Page 1 will show:
    #   START: (1 - 1) * 16 + 1 = 1
    #   END: 1 * 16 = 16
    # Page 2 would show:
    #   START: (2 - 1) * 16 + 1 = 17
    #   END: 2 * 16 = 32
    $barcodes_per_page = getSetting('BARCODES_PER_PAGE', $db, '16');
    $start_record = ($pagenum - 1) * $barcodes_per_page + 1;
    $end_record = $pagenum * $barcodes_per_page;
    # Select barcodes based on the limit
    if ($_SESSION['access'] == ACCESS_ADMINISTRATOR) {
        $stmt = $db->query("SELECT key_barcode, description FROM keyring ORDER BY key_barcode LIMIT $barcodes_per_page OFFSET $start_record");
    } else {
        $allowed_depts = $_SESSION['user_info']['groups'];
        $questionmarks = str_repeat("?,", count($allowed_depts)-1) . "?";
        $stmt = $db->prepare("SELECT key_barcode, description
                                        FROM keyring
                                        LEFT JOIN keyring_depts ON keyring.key_barcode = keyring_depts.keyring_id
                                        WHERE depts_id IN ($questionmarks)
                                        ORDER BY key_barcode LIMIT $barcodes_per_page OFFSET $start_record");
        $stmt->execute($allowed_depts);
    }
    # Set up the next and back buttons
    print("<div class='bc-page-nav'>\n");
    if ($pagenum > 1) {
        $prev_page = $pagenum - 1;
        print("<a href='?pagenum=$prev_page' class='btn btn-primary'>PREV PAGE</a>");
    }
    print("</div>\n");
    ?>
    <div class="barcode-table">
        <table class="table-bordered">
            <tr>
        <?php
        $cnt = 0;
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $cnt++;
            $code_type = 'code128b';
            $code_size = 52;
            $print_text = 'true';
            $img_url = "../include/php-barcode/barcode.php?text=" . $row['key_barcode'] . "&codetype=" . $code_type . "&size=" . $code_size . "&print=" . $print_text;
            $max_length = 50;
            if (strlen($row['description']) > $max_length) {
                $key_desc = trim(substr($row['description'], 0, $max_length)) . "&hellip;";
            } else {
                $key_desc = $row['description'];
            }
            ?>
                <td class="barcode-box">
                    <div class="barcode-description"><?= $key_desc ?></div>
                    <img alt="<?= $key_desc ?>" src="<?= $img_url ?>" height="84">
                </td>
                <td class="barcode-box">
                    <div class="barcode-description"><?= $key_desc ?></div>
                    <img alt="<?= $key_desc ?>" src="<?= $img_url ?>" height="84">
                </td>
                <?php
            if ($cnt % 2 == 0) {
                echo "</tr>\n<tr>\n";
            } else {
                echo "<td class='barcode-table-col2'></td>\n";
            }
        }
        ?>
        </table>
    </div>
    <?php
    if ($cnt >= $barcodes_per_page) {
        print("<div class='bc-page-nav'>\n");
        $next_page = $pagenum + 1;
        print("<a href='?pagenum=$next_page' class='btn btn-primary'>NEXT PAGE</a>");
        print("</div>\n");
    }
} else {
    echo "<p>You are not authorized to access this page.  Please log in first.</p>";
}
require_once "../include/footer.php";
