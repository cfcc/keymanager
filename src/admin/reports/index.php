<?php
/**
 * Created by PhpStorm.
 * User: jfriant
 * Date: 8/17/16
 * Time: 10:55 AM
 */
$page_title = "Admin:Reports";
$include_path = "../../";
require_once("../../include/app_config.php");
require_once("../../include/database.php");
require_once("../../include/functions.php");

$config = load_config();
$db = connect_key_db($config);

sec_session_start();

require_once("../../include/header.php");

if (login_check($config, $db, ACCESS_USER)) {
?>
<div class="row">
    <!-- Note that users should not normally get to this page -->
    <p>Select a report from the menu above.</p>
</div>
<?php
} else {
    echo "<p>You are not authorized to access this page.  Please login first.</p>";
}
require_once '../../include/footer.php';
