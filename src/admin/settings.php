<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/11/16
 * Time: 1:48 PM
 */
$page_title = "Admin:Settings";
$include_path = "../";
$logout_url = "../index.php";
$js_include = array(
    'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'
);
$page_css = array(
    'https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
);
$page_script = array(
    "$(document).ready(function() {",
    "  $('#settings_list').DataTable( { searching: false } );",
    "} );"
);
require_once "../include/app_config.php";
require_once '../include/database.php';
require_once "../include/functions.php";

$config = load_config();
$db = connect_key_db($config);

sec_session_start();

require_once "../include/header.php";

if (login_check($config, $db, ACCESS_MANAGER)) {
    if (array_key_exists('id', $_GET)) {
        # only administrators can change global settings
        if ($_SESSION['access'] == ACCESS_ADMINISTRATOR) {
            $stmt = $db->prepare('SELECT * FROM settings WHERE id=?');
            $stmt->execute(array($_GET['id']));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($result) {
                $record = $result[0];
            } else {
                $record = array(
                    'id' => '',
                    'value' => '',
                    'description' => ''
                );
            }
            if ($_GET['action'] == 'edit') {
                ?>
                <form action="settings.php" autocomplete="off" class="small-form">
                    <input type="hidden" name="action" value="save">
                    <input type="hidden" name="current_id" value="<?php echo $record['id'] ?>">
                    <div class="form-group">
                        <label for="id">ID</label>
                        <input type="text" maxlength="20" id="id" name="id" value="<?php echo $record['id']; ?>"
                               class="form-control" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="value">Value</label>
                        <input type="text" maxlength="50" id="value" name="value"
                               value="<?php echo $record['value']; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" id="description" name="description"
                               value="<?php echo $record['description']; ?>" class="form-control">
                    </div>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="settings.php" class="btn btn-default">Cancel</a>
                    </div>
                </form>
                <?php
            } elseif ($_GET['action'] == 'delete') {
                if (empty($_GET['confirm'])) {
                    echo "<h3>Confirm Deletion</h3>\n";
                    echo "<p>Are you sure you want to remove the setting: " . $_GET['id'] . "</p>\n";
                    echo "<div class='btn-group'>";
                    echo "<a href=\"settings.php?action=delete&id=" . $_GET['id'] . "&confirm=true\" class=\"btn btn-default\">Delete</a>\n";
                    echo "<a href='settings.php' class='btn btn-default'>Cancel</a>";
                    echo "</div>";
                } else {
                    $stmt = $db->prepare("DELETE FROM settings WHERE id=?");
                    $stmt->execute(array($_GET['id']));
                    $records_changed = $stmt->rowCount();
                    echo "<p>Deleted " . $records_changed . " record</p>\n";
                    echo "<div class='btn-group'>";
                    echo "<a href='settings.php' class='btn btn-default'>Continue</a>";
                    echo "</div>\n";
                }
            } elseif ($_GET['action'] == 'save') {
                $records_changed = 0;
                if (!empty($_GET['current_id'])) {
                    $stmt = $db->prepare("UPDATE settings SET id=?, value=?, description=? WHERE id=?");
                    $stmt->execute(array($_GET['id'], $_GET['value'], $_GET['description'], $_GET['current_id']));
                    $records_changed = $stmt->rowCount();
                } else {
                    $stmt = $db->prepare("INSERT INTO settings (id, value, description) VALUES (?, ?, ?)");
                    $stmt->execute(array($_GET['id'], $_GET['value'], $_GET['description']));
                    $records_changed = $stmt->rowCount();
                }
                echo "<p>Updated records: " . $records_changed . "</p>\n";
                echo "<p><a href='settings.php' class='btn btn-default'>Continue</a></p>";
            }
        } else {
            echo "<h2>Access Denied</h2>\n";
            echo "<p>You do not have authorization to change that setting</p>\n";
            echo "<p><a href='settings.php' class='btn btn-default'>Back</a>\n";
        }
    } else {
        ?>
        <div class="col-md-2 col-md-offset-10">
            <p><a href="settings.php?action=edit&id=" class="btn btn-default">Add</a></p>
        </div>
        <table id="settings_list" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Value</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $stmt = $db->query("SELECT id, value, description FROM settings ORDER BY id");
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                ?>
                <tr>
                    <td><?php echo $row['id']; ?></td>
                    <td><?php echo $row['value']; ?></td>
                    <td><?php echo $row['description']; ?></td>
                    <td>
                        <?php if ($_SESSION['access'] == ACCESS_ADMINISTRATOR) { ?>
                            <div class="btn-group">
                                <a href="settings.php?action=edit&id=<?php echo $row['id'] ?>" class="btn btn-default">Edit</a>
                                <a href="settings.php?action=delete&id=<?php echo $row['id'] ?>" class="btn btn-default">Delete</a>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <?php
    }
} else {
    echo "<p>You are not authorized to access this page.  Please login first.</p>";
}
require_once '../include/footer.php';
