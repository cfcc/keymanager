<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/7/16
 * Time: 9:20 AM
 */

require_once "app_config.php";
require_once "database.php";
require_once "functions.php";

$config = load_config();
$db = connect_key_db($config);

# this allows us to set the relative path if necessary
$rel_path = $include_path ?? "";

if (!isset($logout_url)) { $logout_url = ""; }

if (empty($menu)) {
    $menu = array(
        'Home' => 'index.php',
        'Activity Report' => 'report.php',
        'Dashboard' => 'dashboard.php',
    );
}
# used for non-managers to give access to barcodes
if (empty($menu_barcode)) {
    $menu_barcode = array(
        'Barcodes' => 'admin/barcodes.php'
    );
}
if (empty($manager_menu)) {
    $manager_menu = array(
        'Admin' => array(
            'Barcodes' => 'admin/barcodes.php',
            'sep1' => '-',
            'Edit Keyring' => 'admin/keyring.php',
            'Edit Staff' => 'admin/staff.php',
            'Edit Station' => 'admin/station.php',
            'sep2' => '-',
            'Supervisor Notification Report' => 'admin/notify.php',
            'Supervisor Notification History' => 'admin/notify.php?history=',
            'sep3' => '-',
            'Settings' => 'admin/settings.php'
        ),
    );
}
if (empty($admin2_menu)) {
    $admin2_menu = array(
            'Admin2' => array(
                    'Barcode Labels' => 'admin/barcode_labels.php',
                'Activity Report' => 'admin/reports/activity.php',
                'Import Staff Names' => 'admin/import_staff_names.php',
                'Keyring Status Report' => 'admin/reports/keystatus.php',
            ),
    );
}
if (!empty($page_title)) {
    $active = $page_title;
} else { 
    $active = 'Home'; 
}

function generate_menu($menu_items, $active_label, $rel_path) {
    $html_out = "";
    foreach ($menu_items as $label => $url) {
        if (is_array($url)) {
            $html_out .= "<li class='dropdown'>\n";
            $html_out .= "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>$label <span class='caret'></span></a>\n";
            $html_out .= "<ul class='dropdown-menu'>\n";
            foreach ($url as $label2 => $url2) {
                if ($url2 == '-') {
                    $html_out .= "<li role=\"separator\" class=\"divider\"></li>\n";
                } else {
                    $html_out .= "<li><a href='". $rel_path . $url2 . "'>" . $label2 . "</a></li>\n";
                }
            }
            $html_out .= "</ul>\n";
            $html_out .= "</li>\n";
        } else {
            if ($url == '-') {
                $html_out .= '<li role="separator" class="divider"></li>\n';
            } else {
                if ($active_label == $label) {
                    $html_out .= "<li class='active'><a href='#'>" . $label . "</a></li>\n";
                } else {
                    $html_out .= "<li><a href='" . $rel_path . $url . "'>" . $label . "</a></li>\n";
                }
            }
        }
    }
    return $html_out;
}
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Key Manager</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <?php
                echo generate_menu($menu, $active, $rel_path);
                if (login_check($config, $db, ACCESS_MANAGER)) {
                    echo generate_menu($manager_menu, $active, $rel_path);
                    # If this is an Administrator also generate the second admin menu (mainly used for testing)
                    if ($_SESSION['access'] >= ACCESS_ADMINISTRATOR) {
                        echo generate_menu($admin2_menu, $active, $rel_path);
                    }
                } else {
                    # logged-in users who do not see the admin menu get the barcodes
                    if (login_check($config, $db, ACCESS_USER) == true) {
                        echo generate_menu($menu_barcode, $active, $rel_path);
                    }
                }
                ?>
            </ul>
            <?php
            if (empty($hide_logon_form)) {
                $result = login_check($config, $db);
                echo "<form class=\"navbar-form navbar-right\">\n";
                if ($result) {
                    echo "<div class=\"form-group\">\n";
                    echo "<input type=\"button\" class=\"btn btn-default\" onclick=\"do_logout('" . $rel_path . "', '" . $logout_url . "');\" value=\"Logout\">\n";
                    echo "</div>\n";
                } else {
                    ?>
                    <!-- the login form starts here -->
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" placeholder="Username" onkeypress="can_submit(event, '<?php echo $rel_path ?>');">
                        <input type="password" class="form-control" id="password" placeholder="Password" onkeypress="return can_submit(event, '<?php echo $rel_path ?>');">
                    </div>
                    <input type="button" class="btn btn-default" onclick="authenticate('', '<?php echo $rel_path ?>');" value="Login">
                    <?php
                }
                echo "</form>\n";
                if ($result) {
                    echo "<p class=\"navbar-text navbar-right\">Signed in as " . $_SESSION['user_info']['username'] . "</p>";
                }
            } else {
                echo "<p class=\"navbar-text navbar-right\">Station $station_id</p>\n";
            }
            ?>
        </div><!--/.nav-collapse -->
    </div>
</nav>
