<?php

function get_table_creation_queries(): array
{
    $db_sql = array(
        'keyring' => "CREATE TABLE `keyring` (
  `key_barcode` varchar(12) NOT NULL,
  `description` varchar(100) DEFAULT '',
  PRIMARY KEY (`key_barcode`),
  UNIQUE KEY `keyring_key_barcode_uindex` (`key_barcode`)
)",
        'keyring_depts' => "CREATE TABLE `keyring_depts` (
  `keyring_id` varchar(12) NOT NULL,
  `depts_id` varchar(5) NOT NULL,
  PRIMARY KEY (`keyring_id`,`depts_id`)
)",
        'login_attempts' => "CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL,
  PRIMARY KEY (`user_id`)
)",
        'members' => "CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  PRIMARY KEY (`id`)
)",
        'settings' => "CREATE TABLE `settings` (
  `id` varchar(20) NOT NULL,
  `value` varchar(50) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
)",
        'staff' => "CREATE TABLE `staff` (
  `id_barcode` bigint(20) NOT NULL,
  `colleague_id` varchar(7) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `last_name` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `supervisor_email` varchar(100) DEFAULT NULL,
  `alt_dept` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_barcode`)
)",
        'stations' => "CREATE TABLE `stations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` text,
  `name` varchar(100) NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `ip_address` varbinary(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `stations_id_uindex` (`id`)
)",
        'stations_depts' => "CREATE TABLE `stations_depts` (
  `stations_id` int(11) NOT NULL,
  `depts_id` varchar(5) NOT NULL,
  PRIMARY KEY (`stations_id`,`depts_id`)
)",
        'checkout_log' => "CREATE TABLE `checkout_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `out_timestamp` datetime DEFAULT NULL,
  `out_station` int(11) DEFAULT NULL,
  `in_timestamp` datetime DEFAULT NULL,
  `in_station` int(11) DEFAULT NULL,
  `keyring_id` varchar(12) NOT NULL,
  `staff_id` bigint(20) NOT NULL,
  `alerts_sent` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `checkout_log_keyring_key_barcode_fk` (`keyring_id`),
  KEY `checkout_log_staff_id_barcode_fk` (`staff_id`),
  CONSTRAINT `checkout_log_keyring_key_barcode_fk` FOREIGN KEY (`keyring_id`) REFERENCES `keyring` (`key_barcode`),
  CONSTRAINT `checkout_log_staff_id_barcode_fk` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id_barcode`)
)"
    );
    return $db_sql;
}