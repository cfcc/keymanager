DROP TABLE checkout_log;
DROP TABLE keyring;
DROP TABLE staff;
CREATE TABLE keyring
(
  `key_barcode` VARCHAR(12) PRIMARY KEY NOT NULL,
  `description` VARCHAR(100) DEFAULT ''
);
CREATE UNIQUE INDEX keyring_key_barcode_uindex ON keyring (key_barcode);
CREATE TABLE staff
(
  `id_barcode` BIGINT PRIMARY KEY NOT NULL,
  `colleague_id` VARCHAR(7),
  `is_active` BOOLEAN DEFAULT TRUE,
  -- All the person information is pulled from ODS if they have a Colleague ID
  last_name varchar(50) default null,
  first_name varchar(50) default null,
  supervisor_email varchar(100) default null,
  alt_dept varchar(5) default null
);
CREATE TABLE checkout_log
(
  id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  out_timestamp DATETIME,
  in_timestamp DATETIME,
  keyring_id VARCHAR(12) NOT NULL,
  staff_id BIGINT(20) NOT NULL,
  out_station INT(11),
  in_station INT(11),
  alerts_sent INT(11) DEFAULT '0',
  CONSTRAINT checkout_log_keyring_key_barcode_fk FOREIGN KEY (keyring_id) REFERENCES keyring (key_barcode),
  CONSTRAINT checkout_log_staff_id_barcode_fk FOREIGN KEY (staff_id) REFERENCES staff (id_barcode)
);
CREATE INDEX checkout_log_keyring_key_barcode_fk ON checkout_log (keyring_id);
CREATE INDEX checkout_log_staff_id_barcode_fk ON checkout_log (staff_id);
CREATE TABLE members (
  `id`       INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `username` VARCHAR(30) NOT NULL,
  `email`    VARCHAR(50) NOT NULL,
  `password` CHAR(128)   NOT NULL
);
CREATE TABLE login_attempts
(
  `user_id` INT(11) PRIMARY KEY NOT NULL,
  `time` VARCHAR(30) NOT NULL
);
CREATE TABLE settings
(
  `id` VARCHAR(20) NOT NULL UNIQUE PRIMARY KEY,
  `value` VARCHAR(50) DEFAULT NULL,
  `description` TEXT
);
CREATE TABLE stations
(
  id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  uid TEXT,
  name VARCHAR(100) NOT NULL,
  is_active BOOLEAN DEFAULT FALSE
);
CREATE UNIQUE INDEX stations_id_uindex ON stations (id);
-- These "link" the stations and keyrings to the ODS_DEPTS table
CREATE TABLE stations_depts
(
  stations_id INT(11) NOT NULL,
  depts_id VARCHAR(5) NOT NULL,
  CONSTRAINT `PRIMARY` PRIMARY KEY (stations_id, depts_id)
);
CREATE TABLE keyring_depts
(
  keyring_id VARCHAR(12) NOT NULL,
  depts_id VARCHAR(5) NOT NULL,
  CONSTRAINT `PRIMARY` PRIMARY KEY (keyring_id, depts_id)
);
-- Create some sample data
-- INSERT INTO staff (id_barcode, colleague_id) VALUES (23177001317469, '0075580');
-- INSERT INTO keyring (key_barcode, comment, checked_out) VALUES ('KEY NORTH 01', "Test keyring", FALSE);

