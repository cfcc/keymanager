<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/13/16
 * Time: 8:46 AM
 *
 * This is intended as an alternate (secure) way to check out a key.  Instead of scanning your ID's barcode,
 * you would login to AD.
 *
 */
header("content-type:application/json");

include_once '../../include/app_config.php';
include_once '../../include/database.php';
include_once '../../include/functions.php';

$config = load_config();

$json_out = array(
    'id_barcode' => null
);

if (isset($_POST['username']) && isset($_POST['password'])) {
    $ldap = ldap_connect($config['ad']['hostname']);

    $username = $_POST['username'];
    $password = $_POST['password'];

    $ldaprdn = 'AD' . '\\' . $username;

    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

    $bind = @ldap_bind($ldap, $ldaprdn, $password);

    if ($bind) {
        $filter = "(sAMAccountName=$username)";
        $result = ldap_search($ldap, "dc=AD,dc=CFCC,dc=EDU", $filter);
        ldap_sort($ldap,$result,"employeeNumber");
        $info = ldap_get_entries($ldap, $result);
        if ($info['count'] >= 1) {
            $json_out['id_barcode'] = getBarcodeWithColleagueId($info['0']['employeeNumber'][0], $db);
        }
    }
    @ldap_close($ldap);
}

echo json_encode($json_out);

exit();