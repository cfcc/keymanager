<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Installation and Configuration</title>
    <style>
        #radioButtons{
            margin: 5px 0 10px 0;
        }

        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=password], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit] {
            width: 100%;
            background-color: #016a70;
            color: white;
            padding: 14px 20px;
            margin-top: 12px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover {
            background-color: #018c94;
        }

        div {
            margin: auto;
            width: 30%;
            border-radius: 5px;
            background-color: #ededed;
            padding: 20px;
            font-family: 'Work Sans', sans-serif;
        }
    </style>
</head>
<body>
<?php
if (empty($_POST['submit'])) {
 ?>
<div>
    <form method="post">

        <label for="host">MySQL Database Host</label>
        <input type="text" id="host" name="host" placeholder="localhost">

        <label for="username">MySQL Database Username</label>
        <input type="text" id="username" name="username" placeholder="username">

        <label for="db_password">MySQL Database Password</label>
        <input type="password" id="db_password" name="db_password">

        <label for="db_name">MySQL Database Name</label>
        <input type="text" id="db_name" name="db_name" placeholder="db_name">

        <hr>

        <label for="ods_host">ODS Database Host</label>
        <input type="text" id="ods_host" name="ods_host" placeholder="localhost">

        <label for="ods_username">ODS Database Username</label>
        <input type="text" id="ods_username" name="ods_username" placeholder="username">

        <label for="ods_password">ODS Database Password</label>
        <input type="password" id="ods_password" name="ods_password">

        <label for="ods_db_name">ODS Database Name</label>
        <input type="text" id="ods_db_name" name="ods_db_name" placeholder="db_name">

        <hr>

        <label for="ad_host">Active Directory Domain Controller FQDN</label>
        <input type="text" id="ad_host" name="ad_host" placeholder="server.ad.example.com">

        <label for="user_dn">User Search Base</label>
        <input type="text" id="user_dn" name="user_dn" placeholder="ou=users,dc=ad,dc=example,dc=com">

        <label for="domain">AD Domain</label>
        <input type="text" id="domain" name="domain" placeholder="ad.example.com">

        <label for="user_group">User Group for Allowed Logins</label>
        <input type="text" id="user_group" name="user_group" placeholder="groupname">

        <label for="manager_group">User Group for Managers</label>
        <input type="text" id="manager_group" name="manager_group" placeholder="groupname">

        <label for="admin_group">User Group for Administrators</label>
        <input type="text" id="admin_group" name="admin_group" placeholder="groupname">

        <input type="submit" name="submit" value="Submit">
    </form>
</div>
<?php
/**
 * If the application has not been configured yet, then this can do the initial set up of the database.
 *
 * Ideas from: https://www.blogdesire.com/how-to-create-php-script-installer-using-php/
 */
} else {
    $options = array(
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );

    $mysql_db = null;
    $ods_db = null;
    $default_db = null;

    $charset = 'utf8';

    echo "<p>Testing connection to database server " . $_POST['host'] . "...</p>";

    $db_error = true;
    try {
        # $dsn = "mysql:host=" . $_POST['host'] . ";dbname=" . $_POST['db_name'] . ";charset=" . $charset;
        $dsn = "mysql:host=" . $_POST['host'] . ";dbname=mysql;charset=utf8";
        $mysql_db = new PDO($dsn, $_POST['username'], $_POST['db_password'], $options);
        $db_error = false;
        echo "<p>SUCCESS</p>";
    } catch (Exception $e) {
        $fatal_error = print_r($e->getMessage(), true) . " for MySQL Database";
        echo $fatal_error;
    }

    echo "<p>Testing connection to ODS server " . $_POST['host'] . "...</p>";

    $ods_error = true;
    try {
        $dsn = "sqlsrv:server=".$_POST['ods_host'].";Database = ".$_POST['ods_db_name'].";Encrypt=true;TrustServerCertificate=true";
        $ods_db = new PDO($dsn, $_POST['ods_username'], $_POST['ods_password']);
        $ods_db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        $ods_error = false;
        echo "<p>SUCCESS</p>";
        $ods_db = null;
    } catch (Exception $e) {
        $fatal_error = print_r($e->getMessage(), true) . " for MS SQL Database";
        echo $fatal_error;
    }

    if (!$db_error && !$ods_error) {
        #
        # Create the initial database and then reconnect to it
        #
        try {
            $query = "CREATE DATABASE IF NOT EXISTS " . $_POST['db_name'];
            $mysql_db->exec($query);
            $is_database_ready = true;
            echo "<p>Database created successfully.</p>";
        } catch (Exception $e) {
            $is_database_ready = false;
            echo "<p>Database creation failed</br>ERROR: " . $e->getMessage() . "</p>";
        }

        if ($is_database_ready) {
            require_once "install_db.php";
            require_once "../include/app_config.php";
            $are_tables_ready = true;
            $dsn = "mysql:host=" . $_POST['host'] . ";dbname=" . $_POST['db_name'] . ";charset=" . $charset;
            $conn = new PDO($dsn, $_POST['username'], $_POST['db_password'], $options);
            $db_sql = get_table_creation_queries();
            echo "<p>Creating the database tables...</p>";
            try {
                echo "<ul>";
                foreach ($db_sql as $table_name => $query) {
                    echo "<li>$table_name</li>";
                    $conn->exec($query);
                }
            } catch (PDOException $e) {
                $are_tables_ready = false;
                echo "<li><b>ERROR</b><br>$query<br>\n" . $e->getMessage() . "</li>";
            }
            echo "</ul>\n";
            $conn = null;
            if ($are_tables_ready) {
                $settings = "[database]
host = " . $_POST['host'] . "
dbname = " . $_POST['db_name'] . "
username = " . $_POST['username']. "
password = " . $_POST['db_password']. "
charset = utf8mb4

[ods]
host = " . $_POST['ods_host'] . "
dbname = " . $_POST['ods_db_name'] . "
username = " . $_POST['ods_username'] . "
password = " . $_POST['ods_password'] . "
charset = utf8mb4

[ad]
hostname = " . $_POST['ad_host'] . "
user_dn = " . $_POST['user_dn'] . "
domain = " . $_POST['domain'] . "
user_groups[] = " . $_POST['user_group'] . "
manager_group[] = " . $_POST['manager_group'] . "
administrator_groups[] = " . $_POST['admin_group'] . "
";
                $authentication_module = "authenticationModule_domain_controller=" . $_POST['ad_host'] . "
authenticationModule_base_dn=" . $_POST['user_dn'] . "
authenticationModule_account_suffix=@" . $_POST['domain'] . "
";
                if (save_config($settings) ) {
                    echo "<p>Configuration saved successfully.</p>";
                } else {
                    echo "<hr>";
                    echo "Please save the following settings to the settings.ini file under the instance directory.";
                    echo "<pre>";
                    echo $settings;
                    echo "</pre>";
                    echo "Then create the authentication_module.ini configuration file under the instance directory and add these.";
                    echo "<pre>";
                    echo $authentication_module;
                    echo "</pre>";
                }
            }
        }
    } else {
        echo "<p>One or more database connections failed.  Please correct the errors and <a href='install.php'>try again</a>.</p>";
    }
}

?>
</body>
</html>
