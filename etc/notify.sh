#!/bin/sh
#
# Install this in /etc/cron.hourly to generate the "key late" notifications
#
INSTALL_PATH='/var/www/html/admin/'

cd ${INSTALL_PATH} || exit 1
/usr/local/bin/php -q notify.php > /var/log/notify.log
