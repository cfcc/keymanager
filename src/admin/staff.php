<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/7/16
 * Time: 12:45 PM
 */
$page_title = "Admin:Staff";
$include_path = "../";
$logout_url = "../index.php";
$js_include = array(
    'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',
    '../resources/js/forms.js'
);
$page_css = array(
    'https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
);
$page_script = array(
    "$(document).ready(function() {",
    "  $('#staff_list').DataTable( { searching: false } );",
    "} );",
    "const inputs = document.querySelectorAll(\"input, select, textarea\");",
    "inputs.forEach(input => {",
    "  input.addEventListener(",
    "    \"invalid\",",
    "    event => {",
    "      input.classList.add(\"error\");",
    "    },",
    "    false",
    "  );",
    "});"
);

require_once "../include/app_config.php";
require_once "../include/database.php";
require_once "../include/functions.php";

$config = load_config();
$db = connect_key_db($config);
$ods_db = connect_ods_db($config);

sec_session_start();

require_once "../include/header.php";

if (login_check($config, $db, ACCESS_MANAGER)) {
    if (array_key_exists('id_barcode', $_GET)) {
        $access_allowed = false;
        if ( $_GET['id_barcode'] != "") {
            $stmt = $db->prepare("SELECT * FROM staff WHERE id_barcode=?");
            $stmt->execute(array($_GET['id_barcode']));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $result = null;
        }
        if ($result && is_array($result)) {
            $record = $result[0];
            # now we need to make sure the user has access to change this record
            if ($_SESSION['access'] == ACCESS_ADMINISTRATOR) {
                # by default Administrators have access to everything
                $access_allowed = true;
            } else {
                if ($record['colleague_id']) {
                    $stmt2 = $ods_db->prepare("SELECT HRP_PRI_DEPT_SORT FROM ODS_HRPER WHERE HRPER_ID=?");
                    $stmt2->execute(array($record['colleague_id']));
                    $ods_row = $stmt2->fetch(PDO::FETCH_ASSOC);
                    $this_rec_dept = $ods_row['HRP_PRI_DEPT_SORT'];
                } else {
                    $this_rec_dept = $record['alt_dept'];
                }
                if (in_array($this_rec_dept, $_SESSION['user_info']['groups'])) {
                    $access_allowed = true;
                }
            }
        } else {
            // if the database query returned nothing, then we'll set up a blank record
            $access_allowed = true;
            // grab logged-in user's information
            $user_info = getStaffInfo($ods_db, $_SESSION['user_info']['employeenumber']);
            $user_dept = $user_info['HRP_PRI_DEPT_SORT'];
            $user_email = $_SESSION['user_info']['username'] . "@mail.cfcc.edu";
            $record = array(
                'id_barcode' => '',
                'colleague_id' => '',
                'first_name' => '',
                'last_name' => '',
                'alt_dept' => $user_dept,
                'supervisor_email' => $user_email,
                'is_active' => 1,
            );
        }
        if ($access_allowed) {
            if ($_GET['action'] == 'edit') {
                if ($record['colleague_id']) {
                    $result = getStaffInfo($ods_db, $record['colleague_id']);
                    $first_name = $result['FIRST_NAME'];
                    $last_name = $result['LAST_NAME'];
                    $alt_dept = $result['HRP_PRI_DEPT_SORT'];
                    $supervisor_email = getSupervisorEmail($ods_db, $record['colleague_id']);
                } else {
                    $first_name = $record['first_name'];
                    $last_name = $record['last_name'];
                    $alt_dept = $record['alt_dept'];
                    $supervisor_email = $record['supervisor_email'];
                }
                ?>
                <form action="staff.php" autocomplete="off" class="small-form">
                    <input type="hidden" name="action" value="save">
                    <input type="hidden" name="current_id_barcode" value="<?php echo $record['id_barcode'] ?>"
                           class="form-control">
                    <div class="form-group">
                        <label for="id_barcode">ID Barcode</label>
                        <input type="number" id="id_barcode" name="id_barcode" value="<?php echo $record['id_barcode'] ?>"
                               class="form-control" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="colleague_id">Colleague ID</label>
                        <input type="text" id="colleague_id" name="colleague_id" maxlength="7" placeholder="9999999"
                               value="<?php echo $record['colleague_id'] ?>" class="form-control" onchange="toggleNameFields(this.value);">
                    </div>
                    <?php
                    if ($record['colleague_id'] != "") { $disabled = "disabled"; } else { $disabled = ""; }
                    ?>
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" id="first_name" name="first_name" <?php echo $disabled; ?>
                               value="<?php echo $first_name ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" id="last_name" name="last_name" <?php echo $disabled; ?>
                               value="<?php echo $last_name ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="supervisor_email">Supervisor Email</label>
                        <input type="email" id="supervisor_email" name="supervisor_email" <?php echo $disabled; ?>
                               value="<?php echo $supervisor_email ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="dept_code">Department Code</label>
                        <input type="text" id="dept_code" name="dept_code" <?php echo $disabled; ?>
                               value="<?php echo $alt_dept ?>" class="form-control" maxlength="5" required>
                    </div>
                    <div class="form-group">
                        <label for="is_active">Active?</label>
                        <input type="checkbox" id="is_active" name="is_active"
                            <?php if ($record['is_active']) {
                                echo ' checked';
                            } ?> class="form-control">
                    </div>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="staff.php" class="btn btn-default">Cancel</a>
                    </div>
                </form>
                <?php
            } elseif ($_GET['action'] == 'save') {
                $records_changed = 0;
                if (isset($_GET['is_active'])) {
                    $is_active = 1;
                } else {
                    $is_active = 0;
                }
                // FIXME: validate these fields
                $alt_dept = "";
                $first_name = "";
                $last_name = "";
                $supervisor_email = "";
                if (!$record['colleague_id']) {
                    if(isset($_GET['dept_code'])) { $alt_dept = $_GET['dept_code']; }
                    if(isset($_GET['first_name'])) { $first_name = $_GET['first_name']; }
                    if(isset($_GET['last_name'])) { $last_name = $_GET['last_name']; }
                    if(isset($_GET['supervisor_email'])) { $supervisor_email = $_GET['supervisor_email']; }
                }
                if (isset($_GET['current_id_barcode']) && $_GET['current_id_barcode'] != "") {
                    try {
                        $stmt = $db->prepare("UPDATE staff SET id_barcode=?, colleague_id=?, last_name=?, first_name=?, supervisor_email=?, alt_dept=?, is_active=? WHERE id_barcode=?");
                        $stmt->execute(array($_GET['id_barcode'], $_GET['colleague_id'], $last_name, $first_name, $supervisor_email, $alt_dept, $is_active, $_GET['current_id_barcode']));
                        $records_changed = $stmt->rowCount();
                    } catch (PDOException $e) {
                        echo "<p class='bg-danger'>Update failed with exception: " . $e . "</p><pre>";
                        print_r($_GET);
                        echo "</pre>";
                    }
                } else {
                    try {
                        $stmt = $db->prepare("INSERT INTO staff (id_barcode, colleague_id, last_name, first_name, supervisor_email, alt_dept, is_active) VALUES (?, ?, ?, ?, ?, ?, ?)");
                        $stmt->execute(array($_GET['id_barcode'], $_GET['colleague_id'], $last_name, $first_name, $supervisor_email, $alt_dept, $is_active));
                        $records_changed = $stmt->rowCount();
                    } catch (PDOException $e) {
                        echo "<p class='bg-danger'>Insert failed with exception: " . $e . "</p><pre>";
                        print_r($_GET);
                        echo "</pre>";
                    }
                }
                echo "<p>Updated records: " . $records_changed . "</p>\n";
                echo "<p><a href='staff.php' class='btn btn-default'>Continue</a></p>\n";
            } elseif ($_GET['action'] == 'delete') {
                echo "<p>Delete is not supported yet</p>\n";
                echo "<div class='btn-group'>";
                echo "<a href='staff.php' class='btn btn-default'>Cancel</a>";
                echo "</div>\n";
            }
        } else {
            echo "<h2>Access Denied</h2>\n";
            echo "<p>You don't have access to modify that person.</p>\n";
            echo ">Back</a></p>\n";
        }
    } else {
        $staff_list = array();
        $stmt2 = $ods_db->prepare("SELECT ID, FIRST_NAME, LAST_NAME, HRP_PRI_DEPT_SORT FROM ODS_Person, ODS_HRPER WHERE ID=HRPER_ID AND ID=?");
        $stmt = $db->query("SELECT * FROM staff");
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($row['colleague_id'] != "") {
                $stmt2->execute(array($row['colleague_id']));
                $ods_row = $stmt2->fetch(PDO::FETCH_ASSOC);
                $staff_list[] = array(
                    'last_name' => $ods_row['LAST_NAME'],
                    'first_name' => $ods_row['FIRST_NAME'],
                    'dept' => $ods_row['HRP_PRI_DEPT_SORT'],
                    'id_barcode' => $row['id_barcode'],
                    'colleague_id' => $row['colleague_id'],
                    'supervisor_email' => getSupervisorEmail($ods_db, $row['colleague_id']),
                    'is_active' => $row['is_active']);
            } else {
                $staff_list[] = array(
                        'last_name' => $row['last_name'],
                    'first_name' => $row['first_name'],
                    'dept' => $row['alt_dept'],
                    'id_barcode' => $row['id_barcode'],
                    'supervisor_email' => $row['supervisor_email'],
                    'colleague_id' => "",
                    'is_active' => $row['is_active']
                );
            }
        }
        usort($staff_list, "custom_last_name_sort");
        $allowed_depts = $_SESSION['user_info']['groups'];
            ?>
        <div class="col-md-2 col-md-offset-10">
            <p><a href="staff.php?action=edit&id_barcode=" class="btn btn-default">Add</a></p>
        </div>
        <table id="staff_list" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>ID Barcode</th>
                <th>Colleague ID</th>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Dept</th>
                <th>Supervisor's Email</th>
                <th>Active?</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
        foreach ($staff_list as $row) {
            if ($_SESSION['access'] == ACCESS_ADMINISTRATOR || in_array($row['dept'], $allowed_depts)) {
                ?>
                <tr>
                    <td><?php echo $row['id_barcode'] ?></td>
                    <td><?php echo $row['colleague_id'] ?></td>
                    <td><?php echo $row['last_name'] ?></td>
                    <td><?php echo $row['first_name'] ?></td>
                    <td><?php echo $row['dept'] ?></td>
                    <td><?php echo $row['supervisor_email'] ?></td>
                    <td><?php echo $row['is_active'] ?></td>
                    <td>
                        <div class="btn-group">
                            <a href="staff.php?action=edit&id_barcode=<?php echo $row['id_barcode'] ?>"
                               class="btn btn-default">Edit</a>
                            <a href="staff.php?action=delete&id_barcode=<?php echo $row['id_barcode'] ?>"
                               class="btn btn-default">Delete</a>
                        </div>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
            </tbody>
        </table>
        <?php

    }
} else {
    echo "<p>You are not authorized to access this page.  Please login first.</p>";
}

require_once("../include/footer.php");
