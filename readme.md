Key Manager
===========

This is a web application designed for managing the check-in and
check-out of key rings.  The application uses bar codes stored in a
database to record when the keys were checked-out and back in.

A dashboard-style page shows the current status of all keys, and the
activity report shows the keys that are currently checked out.
 
Generating Bar Codes
--------------------

These can be generated in any way, but I used the GNU barcode program
on Linux to set these up.  For example, first I created a text file with
the data I wanted to be encoded:

    KEY WILM 01
    KEY WILM 02
    KEY NORTH 01
    KEY NORTH 02 

Then I used that as input to generate a sheet of codes:

    barcode -t 4x12+40+40 -o label.ps -i barcodes.txt

Author
------

Jakim Friant

Cape Fear Community College

2016-07-06
