<?php
$page_title = "Admin:Staff";
$include_path = "../";
$logout_url = "../index.php";
$js_include = array(
    'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js',
    '../resources/js/forms.js'
);
$page_css = array(
    'https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
);
require_once "../include/app_config.php";
require_once "../include/database.php";
require_once "../include/functions.php";

$config = load_config();
$db = connect_key_db($config);
$ods_db = connect_ods_db($config);

sec_session_start();

require_once "../include/header.php";

if (login_check($config, $db, ACCESS_MANAGER)) {
    if (array_key_exists('do_update', $_GET)) {
        $stmt = $db->prepare("SELECT * FROM staff WHERE is_active = 1 AND (last_name = '' OR last_name IS NULL)");
        $stmt2 = $ods_db->prepare("SELECT ID, FIRST_NAME, LAST_NAME FROM ODS_Person WHERE ID = ?");
        $stmt3 = $db->prepare("UPDATE staff SET last_name = ?, first_name = ? WHERE id_barcode = ? LIMIT 1");

        echo "<p>Updating records...</p>\n";
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($row['colleague_id']) {
                $stmt2->execute(array($row['colleague_id']));
                $ods_result = $stmt2->fetchAll(PDO::FETCH_ASSOC);
                echo "<pre>".$ods_result[0]['LAST_NAME'].", ".$ods_result[0]['FIRST_NAME'].", ".$row['id_barcode'] ."</pre>";
                $stmt3->execute(array($ods_result[0]['LAST_NAME'], $ods_result[0]['FIRST_NAME'], $row['id_barcode']));
            }
        }
        echo "<p>DONE</p>\n";
    } else {
        echo "<p>This will copy names from ODS for all the active staff so that it will be cached on the local server.</p>\n";
        echo "<p><a href='import_staff_names.php?do_update=1' class='btn btn-default'>Import</a></p>\n";
    }
} else {
    echo "<p>You are not authorized to access this page.  Please login first.</p>";
}

require_once("../include/footer.php");
