<?php
/**
 * Created by PhpStorm.
 * User: jfriant
 * Date: 8/17/16
 * Time: 10:33 AM
 *
 * Administrative report showing all activity from the checkout log, with options to filter by department and by date.
 */

$page_title = "Admin:Activity Report";
$include_path = "../../";

require_once "../../include/app_config.php";
require_once "../../include/comparableinterval.php";
require_once '../../include/database.php';
require_once '../../include/functions.php';

$config = load_config();
$db = connect_key_db($config);
$ods_db = connect_ods_db($config);

sec_session_start();

$page_script = array('$(document).ready( function () {',
    '$(\'#activity\').DataTable();',
    '} );'
);

require_once "../../include/header.php";

if (login_check($config, $db, ACCESS_USER) == true) {
    $query_args = array();

    if (!empty($_REQUEST['all'])) {
        $show_all = " checked";
        $limit = new DateTime('1970-1-1');
    } else {
        $show_all = "";
        $limit = new DateTime('now');
        $limit->modify('last week');
    }
    $query_args[] = $limit->format('Y-m-d');

    if (!empty($_REQUEST['dept'])) {
        $dept_filter = $_REQUEST['dept'];
        $query_args[] = $dept_filter;
    } else {
        $dept_filter = null;
    }

    $max_hours_out = getSetting('MAX_HOURS_OUT', $db, '0');
    $all_dates_limit = getSetting('ALL_DATES_LIMIT', $db, '1000');

    if ($dept_filter != null) {
        $query = "SELECT checkout_log.keyring_id, out_timestamp, S_OUT.name AS out_station_name, in_timestamp, S_IN.name AS in_station_name, colleague_id, last_name, first_name
FROM checkout_log
LEFT JOIN staff ON checkout_log.staff_id = staff.id_barcode
LEFT JOIN stations S_OUT ON S_OUT.id=checkout_log.out_station 
LEFT JOIN stations S_IN ON S_IN.id=checkout_log.in_station
LEFT JOIN keyring_depts ON keyring_depts.keyring_id=checkout_log.keyring_id
WHERE staff_id=staff.id_barcode
AND out_timestamp>?
AND keyring_depts.depts_id=?
GROUP BY checkout_log.keyring_id,out_timestamp
ORDER BY out_timestamp DESC
LIMIT $all_dates_limit";
    } else {
        $query = "SELECT checkout_log.keyring_id, out_timestamp, S_OUT.name AS out_station_name, in_timestamp, S_IN.name AS in_station_name, colleague_id, last_name, first_name
FROM checkout_log 
LEFT JOIN staff ON checkout_log.staff_id = staff.id_barcode 
LEFT JOIN stations S_OUT ON S_OUT.id=checkout_log.out_station 
LEFT JOIN stations S_IN ON S_IN.id=checkout_log.in_station
WHERE staff_id=staff.id_barcode
AND out_timestamp>?
GROUP BY checkout_log.keyring_id,out_timestamp
ORDER BY out_timestamp DESC
LIMIT $all_dates_limit";
    }

    $stmt = $db->prepare($query);

    $stmt2 = $ods_db->prepare("SELECT ID, FIRST_NAME, LAST_NAME FROM ODS_Person WHERE ID = ?");

    $stmt3 = $db->prepare("SELECT DISTINCT depts_id FROM keyring_depts");
    $stmt3->execute();
    $all_depts = $stmt3->fetchAll(PDO::FETCH_ASSOC);

    $page_title = 'Activity Report';

    ?>
    <div class="row">
        <form action="activity.php" method="post" class="form-horizontal">
            <div class="form-group">
                <label for="dept">Filter by Department:</label>
                <select id="dept" name="dept">
                    <option value="0">Select a department...</option>
                    <?php
                    foreach ($all_depts as $rec) {
                        if ($rec['depts_id'] == $dept_filter) { $selected = " selected"; } else { $selected = ""; }
                        echo "<option" . $selected . ">" . $rec['depts_id'] . "</option>\n";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>
                    <input type="checkbox" name="all" class="checkbox-inline" <?= $show_all ?>> Show all dates (up to <?= $all_dates_limit ?> records)
                </label>
            </div>
            <div class="btn-group">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </form>
    </div>
    <table class="table" id="activity">
        <thead>
        <tr>
            <th>Key Ring</th>
            <th>Name</th>
            <th>Check-Out Time</th>
            <th>Station</th>
            <th>Check-In Time</th>
            <th>Station</th>
            <th>Time Out</th>
        </tr>
        </thead>
        <tbody>
        <?php

        $stmt->execute($query_args);
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($row['colleague_id']) {
                $stmt2->execute(array($row['colleague_id']));
                $ods_result = $stmt2->fetchAll(PDO::FETCH_ASSOC);
                $name = $ods_result[0]['FIRST_NAME'] . " " . $ods_result[0]['LAST_NAME'];
            } else {
                $name = $row['first_name'] . ' ' . $row['last_name'];
            }
            $result = checkTimeOut($row['out_timestamp'], $row['in_timestamp'], $max_hours_out);
            if ( $result['diff'] == -1) {
                echo "<tr>\n";
            } else {
                if ($row['in_timestamp']) {
                    echo "<tr class='bg-warning'>\n";
                } else {
                    echo "<tr class='bg-danger'>\n";
                }
            }
            echo "<td>" . $row['keyring_id'] . "</td>";
            echo "<td>" . $name . "</td>";
            echo "<td>" . $row['out_timestamp'] . "</td>";
            echo "<td>" . $row['out_station_name'] . "</td>";
            echo "<td>" . $row['in_timestamp'] . "</td>";
            echo "<td>" . $row['in_station_name'] . "</td>";
            if ($result['hours']->days > 0) {
                echo "<td>" . $result['hours']->format('%a day(s)') . "</td>\n";
            } else {
                echo "<td>" . $result['hours']->format('%H:%I:%S') . "</td>\n";
            }
            echo "</tr>\n";
        }
        ?>
        </tbody>
    </table>
    <?php
} else {
    echo "<p>You are not authorized to access this page.  Please login first.</p>";
}
require_once "../../include/footer.php";

