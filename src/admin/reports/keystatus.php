<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/6/16
 * Time: 2:59 PM
 */
$page_title = "Admin:Keyring Status";
$include_path = "../../";

require_once "../../include/app_config.php";
require_once '../../include/database.php';
require_once '../../include/functions.php';

$config = load_config();
$db = connect_key_db($config);

sec_session_start();

$page_script = array('$(document).ready( function () {',
    '  $(\'#keystatus\').DataTable( { ',
    '      "paging": false,',
    '      "info": false,',
    '    } );',
    '} );'
);

require_once "../../include/header.php";

if (login_check($config, $db, ACCESS_USER)) {
    $query_args = array();

    if (!empty($_REQUEST['dept'])) {
        $dept_filter = $_REQUEST['dept'];
        $query_args[] = $dept_filter;

        $stmt = $db->prepare("SELECT key_barcode, description, GROUP_CONCAT(depts_id ORDER BY depts_id ASC SEPARATOR ',') AS all_depts, (SELECT id FROM checkout_log WHERE in_timestamp IS NULL AND checkout_log.keyring_id=keyring.key_barcode) AS borrower
FROM keyring
LEFT JOIN keyring_depts ON keyring_id=key_barcode
WHERE depts_id=?
GROUP BY key_barcode");
    } else {
        $stmt = $db->prepare("SELECT key_barcode, description, GROUP_CONCAT(depts_id ORDER BY depts_id ASC SEPARATOR ',') AS all_depts, (SELECT id FROM checkout_log WHERE in_timestamp IS NULL AND checkout_log.keyring_id=keyring.key_barcode) AS borrower
FROM keyring
LEFT JOIN keyring_depts ON keyring_id=key_barcode
GROUP BY key_barcode");
    }

    $stmt2 = $db->prepare("SELECT out_timestamp, stations.name AS out_station_name, colleague_id, last_name, first_name
FROM checkout_log
LEFT JOIN staff ON checkout_log.staff_id = staff.id_barcode
LEFT JOIN stations ON out_station=stations.id
WHERE checkout_log.id=?");

    # $stmt3 = $ods_db->prepare("SELECT ID, FIRST_NAME, LAST_NAME FROM ODS_Person WHERE ID = ?");

    $page_title = "Keyring Status";

    ?>
    <table class="table" id="keystatus">
        <thead>
        <tr>
            <th>Barcode ID</th>
            <th>Key Ring</th>
            <th>Department</th>
            <th>Name</th>
            <th>Check-Out Time</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $stmt->execute($query_args);
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            echo "<tr>";
            echo "<td>" . $row['key_barcode'] . "</td>";
            echo "<td>" . $row['description'] . "</td>";
            echo "<td>" . $row['all_depts'] . "</td>";
            if ($row['borrower']) {
                $stmt2->execute(array($row['borrower']));
                $result = $stmt2->fetch(PDO::FETCH_ASSOC);
                $name = $result['first_name'] . ' ' . $result['last_name'];
                echo "<td>" . $name . "</td>";
                echo "<td>" . $result['out_timestamp'];
                if ($result['out_station_name']) {
                    echo " <small>(" . $result['out_station_name'] . ")</small>";
                }
                echo "</td>";
            } else {
                echo "<td class='text-success'>Checked In</td><td></td>";
            }
            echo "</tr>\n";
        }
        ?>
        </tbody>
    </table>

    <?php
} else {
    echo "<p>You are not authorized to access this page.  Please login first.</p>";
}
require_once("../../include/footer.php");
?>