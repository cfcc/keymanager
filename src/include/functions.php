<?php
/**
 * Created by PhpStorm.
 * User: jfriant
 * Date: 7/10/16
 * Time: 8:33 AM
 */

include_once 'password_compat-master/lib/password.php';

// Access levels
const ACCESS_DENIED = 0;
const ACCESS_AUTHENTICATED = 1;
const ACCESS_USER = 2;
const ACCESS_MANAGER = 3;
const ACCESS_ADMINISTRATOR = 4;

if(!function_exists('hash_equals'))
{
    function hash_equals($str1, $str2): bool
    {
        if(strlen($str1) != strlen($str2))
        {
            return false;
        }
        else
        {
            $res = $str1 ^ $str2;
            $ret = 0;
            for($i = strlen($res) - 1; $i >= 0; $i--)
            {
                $ret |= ord($res[$i]);
            }
            return !$ret;
        }
    }
}

// The login functions below come from the tutorial on wikihow:
// http://www.wikihow.com/Create-a-Secure-Login-Script-in-PHP-and-MySQL

function sec_session_start(): void
{
    $session_name = 'sec_session_id';   // Set a custom session name
    // This makes HTTPS required for sessions
    $secure = true;
    // This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"],
        $cookieParams["domain"],
        $secure,
        $httponly);
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();            // Start the PHP session
    session_regenerate_id(true);    // regenerated the session, delete the old one.
}

function login($email, $password, $db) {
    // Using prepared statements means that SQL injection is not possible.
    if ($stmt = $db->prepare("SELECT id, username, password FROM members WHERE email = ? LIMIT 1")) {
        $stmt->execute(array($email));    // Execute the prepared query.
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        syslog(LOG_INFO, "login query result: " . implode(", ", $result));

        // get variables from result.
        if (array_key_exists('id', $result)) {
            $user_id = $result['id'];
            syslog(LOG_INFO, 'user id: ' . $user_id);
        }
        if (array_key_exists('username', $result)) {
            $username = $result['username'];
            syslog(LOG_INFO, 'username: ' . $username);
        } else {
            $username = "";
        }
        if (array_key_exists('password', $result)) {
            $db_password = $result['password'];
        } else {
            $db_password = "";
        }

        if (!empty($user_id)) {
            // If the user exists we check if the account is locked
            // from too many login attempts
            if (checkbrute($user_id, $db)) {
                // Account is locked
                syslog(LOG_WARNING, "Account is locked for user_id: " . $user_id);
                return false;
            } else {
                // Check if the password in the database matches
                // the password the user submitted. We are using
                // the password_verify function to avoid timing attacks.
                if (password_verify($password, $db_password)) {
                    // Password is correct!
                    // Get the user-agent string of the user.
                    $user_browser = $_SERVER['HTTP_USER_AGENT'];
                    // XSS protection as we might print this value
                    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                    $_SESSION['user_id'] = $user_id;
                    // XSS protection as we might print this value
                    $username = preg_replace("/[^a-zA-Z0-9_\-]+/",
                        "",
                        $username);
                    $_SESSION['username'] = $username;
                    $_SESSION['login_string'] = hash('sha512', $db_password . $user_browser);
                    // Login successful.
                    return true;
                } else {
                    // Password is not correct
                    syslog(LOG_WARNING, 'Password is not correct: ' . $password . ", " . $db_password);
                    // We record this attempt in the database
                    $now = time();
                    $stmt = $db->query("INSERT INTO login_attempts(user_id, time) VALUES (?, ?)");
                    $stmt->execute(array($user_id, $now));
                    return false;
                }
            }
        } else {
            // no user exists
            syslog(LOG_CRIT, 'login(): User not found for: ' . $email);
            return false;
        }
    }
}

function checkbrute($user_id, $db) {
    // Get timestamp of current time
    $now = time();

    // All login attempts are counted from the past 2 hours.
    $valid_attempts = $now - (2 * 60 * 60);

    if ($stmt = $db->prepare("SELECT time
                                FROM login_attempts
                                WHERE user_id = ?
                                AND time > '$valid_attempts'")) {
        $stmt->execute(array($user_id));

        // If there have been more than 5 failed logins
        if ($stmt->rowCount() > 5) {
            return true;
        } else {
            return false;
        }
    }
}

function login_check($config, $db, $required_access=ACCESS_AUTHENTICATED): bool
{
    // FIXME: this needs to return something different when the login has succeeded but the user is not authorized by a group
    if (array_key_exists('ad', $config)) {
        return login_check_ldap($required_access);
    } else {
        return login_check_database($db);
    }
}

function login_check_database($db): bool
{
    // Check if all session variables are set
    if (isset($_SESSION['user_id'],
        $_SESSION['username'],
        $_SESSION['login_string'])) {

        $user_id = $_SESSION['user_id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];

        // Get the user-agent string of the user.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];

        if ($stmt = $db->prepare("SELECT password
                                      FROM members
                                      WHERE id = ? LIMIT 1")) {
            // Bind "$user_id" to parameter.
            $stmt->execute(array($user_id));   // Execute the prepared query.

            if ($stmt->rowCount() >= 1) {
                // If the user exists get variables from result.
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                $password = $result['password'];
                $login_check = hash('sha512', $password . $user_browser);

                if (hash_equals($login_check, $login_string) ){
                    // Logged In!!!!
                    return true;
                } else {
                    // Not logged in
                    return false;
                }
            } else {
                // Not logged in
                return false;
            }
        } else {
            // Not logged in
            return false;
        }
    } else {
        // Not logged in
        return false;
    }
}

function login_check_ldap($required_access = ACCESS_AUTHENTICATED): bool
{
    $access = ACCESS_DENIED;
    if (isset($_SESSION['user_info']) && isset($_SESSION['access'])) {
        $access = $_SESSION['access'];
    }
    #error_log("login_check_ldap(): " . $access . " >= " . $required_access);
    return ($access >= $required_access);
}

function esc_url($url) {

    if ('' == $url) {
        return $url;
    }

    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);

    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;

    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }

    $url = str_replace(';//', '://', $url);

    $url = htmlentities($url);

    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);

    if ($url[0] !== '/') {
        // We're only interested in relative links from $_SERVER['PHP_SELF']
        return '';
    } else {
        return $url;
    }
}

/**
 * Compare the user's groups with the access levels in the configuration and return the level of access that the user
 * is authorized for.  By default, they will be logged in but with no extra access.
 *
 * @param $groups - The LDAP groups returned by CFCC-AD-Authentication
 * @param $config - the config file contents
 * @return int - the access level, 0 - 2
 */
function authorize($groups, $config): int
{
    $access = ACCESS_AUTHENTICATED;

    // Active Directory user groups
    $ldap_user_groups = $config['ad']['user_groups'];

    // Active Directory manager groups
    $ldap_manager_groups = $config['ad']['manager_groups'];

    // Active Directory administrator groups
    $ldap_administrator_groups = $config['ad']['administrator_groups'];

    // check groups
    foreach ($groups as $group_name) {
        foreach ($ldap_administrator_groups as $admin_group_name) {
            if ($group_name == $admin_group_name) {
                $access = ACCESS_ADMINISTRATOR;
                break;
            }
        }
        # administrators have full access, no need for further checks
        if ($access == ACCESS_ADMINISTRATOR) {
            break;
        }

        foreach ($ldap_manager_groups as $mgr_group_name) {
            if ($group_name == $mgr_group_name) {
                $access = ACCESS_MANAGER;
                break;
            }
        }
        // is manager, break loop
        if ($access == ACCESS_MANAGER) {
            break;
        }

        // is user, but keep checking for a manager group
        foreach ($ldap_user_groups as $user_group_name) {
            if ($group_name == $user_group_name) $access = ACCESS_USER;
        }
    }
    return $access;
}

function getBarcodeWithColleagueId($colleague_id, $db) {
    $stmt = $db->prepare('SELECT id_barcode FROM staff WHERE colleague_id=? AND is_active=TRUE');
    $stmt->execute(array($colleague_id));
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result[0]['id_barcode'];
}

function custom_last_name_sort($a,$b): int
{
    $result = 0;
    if ($a['last_name'] > $b['last_name']) {
        $result = 1;
    } elseif ($a['last_name'] < $b['last_name']) {
        $result = -1;
    }
    return $result;
}

function getSetting($name, $db, $default='') {
    $setting = $default;
    $stmt = $db->prepare("SELECT value FROM settings WHERE id=?");
    $stmt->execute(array($name));
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if (is_array($result) && count($result) > 0) {
        $setting = $result[0]['value'];
    }
    return $setting;
}

/** Return an array with settings from the database.
 *
 * The records should all begin with a common prefix followed by a plus sign and then a unique suffix.
 * For example:
 *   SAMPLE_SETTING+ONE
 *   SAMPLE_SETTING+TWO
 *   MAX_HOURS_OUT+UIT
 */
function getMultiSetting($prefix, $db, $default='') {
    $name = $prefix . "+%";
    $stmt = $db->prepare("SELECT value FROM settings WHERE id like ?");
    $stmt->execute(array($name));
    $settings = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $settings[$prefix . "+DEFAULT"] = $default;
    return $settings;
}

const SIX_MONTHS_AS_SECONDS = 15552000;

function checkStation($db=null) {
    // Check for the station cookie, and refresh it if it exists
    $station_id = null;
    if (array_key_exists('keymanager', $_COOKIE)) {
        // after finding the cookie, we still need to verify that the database says this station is active
        if ($db != null) {
            $stmt = $db->prepare('SELECT id FROM stations WHERE id = ? AND is_active=TRUE');
            $stmt->execute(array($_COOKIE['keymanager']['station_id']));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (is_array($result) && count($result) > 0) {
                $station_id = $result[0]['id'];
                setcookie('keymanager[station_id]', $station_id, time() + SIX_MONTHS_AS_SECONDS, '/');
            } else {
                // not found, delete any invalid cookies
                setcookie('keymanager[station_id]', null, -1, '/');
                setcookie('keymanager', null, -1, '/');
            }
        }
    } else {
        if ($db != null) {
            # When this is behind a proxy such as Traefik, we have to use the original IP address stored in a different
            # header field
            if (array_key_exists('HTTP_X_REAL_IP', $_SERVER)) {
                $uid = md5($_SERVER['HTTP_X_REAL_IP']);
            } else {
                $uid = md5($_SERVER['REMOTE_ADDR']);
            }
            $stmt = $db->prepare('SELECT id FROM stations WHERE uid=? AND is_active=TRUE');
            $stmt->execute(array($uid));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if (is_array($result) && count($result) > 0) {
                $station_id = $result[0]['id'];
            }
        }
    }
    return $station_id;
}

function checkDepts($station_id, $db): array
{
    // using null as a default when the calling browser is not associated with a station
    $my_depts = array(null);
    $stmt = $db->prepare('SELECT depts_id FROM stations_depts WHERE stations_id=?');
    $stmt->execute(array($station_id));
    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
        $my_depts[] = $row['depts_id'];
    }
    return $my_depts;
}

function getDeptsForStation($station_id, $db, $ods): array
{
    $my_depts = array();
    $stmt2 = $db->prepare('SELECT depts_id FROM stations_depts WHERE stations_id=?');
    $stmt3 = $ods->prepare('SELECT DIVISION_DESC, DEPTS_DESC FROM ODS_DEPTS WHERE DEPTS_ID=?');

    $stmt2->execute(array($station_id));
    $result = $stmt2->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $rec) {
        $stmt3->execute(array($rec['depts_id']));
        $result2 = $stmt3->fetchAll(PDO::FETCH_ASSOC);
        if ($result2) {
            $my_depts[$rec['depts_id']] = $result2[0]['DIVISION_DESC'] . ' - ' . $result2[0]['DEPTS_DESC'];
        }
    }

    return $my_depts;
}

function getDeptsForKeyring($keyring_id, $db, $ods_db): array
{
    $my_depts = array();
    $stmt2 = $db->prepare('SELECT depts_id FROM keyring_depts WHERE keyring_id=?');
    $stmt3 = $ods_db->prepare('SELECT DIVISION_DESC, DEPTS_DESC FROM ODS_DEPTS WHERE DEPTS_ID=?');
    $stmt4 = $db->prepare("SELECT alt_dept FROM staff WHERE alt_dept IS NOT NULL AND alt_dept != '' GROUP BY alt_dept");

    $stmt4->execute();
    $result = $stmt4->fetchAll(PDO::FETCH_ASSOC);
    $local_groups = array();
    foreach ($result as $rec) {
        $local_groups[$rec['alt_dept']] = 'Local Group ' . $rec['alt_dept'];
    }

    $stmt2->execute(array($keyring_id));
    $result = $stmt2->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $rec) {
        $stmt3->execute(array($rec['depts_id']));
        $result2 = $stmt3->fetchAll(PDO::FETCH_ASSOC);
        if ($result2) {
            $my_depts[$rec['depts_id']] = $result2[0]['DIVISION_DESC'] . ' - ' . $result2[0]['DEPTS_DESC'];
        } else {
            if (key_exists($rec['depts_id'], $local_groups)) {
                $my_depts[$rec['depts_id']] = $local_groups[$rec['depts_id']];
            }
        }
    }

    return $my_depts;
}

function getAllDepts($ods_db, $db): array
{
    $all_depts = array();
    $stmt3 = $ods_db->prepare('SELECT DEPTS_ID, DIVISION_DESC, DEPTS_DESC FROM ODS_DEPTS WHERE SCHOOL=? ORDER BY DIVISION_DESC, DEPTS_DESC');
    $stmt3->execute(array('ADSCH'));
    foreach ($stmt3->fetchAll(PDO::FETCH_ASSOC) as $record) {
        $all_depts[$record['DEPTS_ID']] = $record['DIVISION_DESC'] . ' - ' . $record['DEPTS_DESC'] . ' (' . $record['DEPTS_ID'] . ')';
    }

    $stmt4 = $db->prepare("SELECT alt_dept FROM staff WHERE alt_dept IS NOT NULL AND alt_dept != '' GROUP BY alt_dept");
    $stmt4->execute();
    $result = $stmt4->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $rec) {
        if (!key_exists($rec['alt_dept'],$all_depts)) {
            $all_depts[$rec['alt_dept']] = 'Local Group ' . $rec['alt_dept'];
        }
    }
    return $all_depts;
}

function getSupervisorEmail($ods_db, $person_id) {
    $supervisor_email = null;
    $query = "SELECT PREFERRED_EMAIL_ADDRESS FROM CFCC_SUPERVISOR_VIEW WHERE HRPER_ID=?";
    $stmt = $ods_db->prepare($query);
    $stmt->execute(array($person_id));
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if ($result) {
        $supervisor_email = $result[0]['PREFERRED_EMAIL_ADDRESS'];
    }
    return $supervisor_email;
}

function getStaffInfo($ods_db, $person_id) {
    $record = null;
    $stmt = $ods_db->prepare("SELECT ID, FIRST_NAME, LAST_NAME, HRP_PRI_DEPT_SORT FROM ODS_Person, ODS_HRPER WHERE ID=HRPER_ID AND ID=?");
    $stmt->execute(array($person_id));
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if ($result) {
        $record = $result[0];
    }
    return $record;
}


/**
 * @throws DateMalformedStringException
 * @throws DateMalformedIntervalStringException
 */
function checkTimeOut($out_timestamp, $in_timestamp, $max_hours_out): array
{
    $result = array(
        'hours' => 0,
        'diff' => -1
    );
    $dt_start = new DateTime($out_timestamp);
    if ($in_timestamp) {
        $dt_end = new DateTime($in_timestamp);
    } else {
        $dt_end = new DateTime('now');
    }
    $iv_over = new ComparableDateInterval('PT' . $max_hours_out . 'H');
    $result['hours'] = $dt_start->diff($dt_end);
    try {
        $iv_hours = new ComparableDateInterval($result['hours']->format('P%DDT%HH%IM%SS'));
    } catch (Exception $e) {
        // For some reason multiple months will give a negative in the day field, so we'll use the months instead
        // echo "<span>ERROR: for " . $row['keyring_id'] . " (" . print_r($hours, true) . ")</span>";
        $iv_hours = new ComparableDateInterval($result['hours']->format('P%MMT%IM%SS'));
    }
    $result['diff'] = $iv_hours->compare($iv_over) == -1;
    return $result;
}

#
# Conversion steps from https://stackoverflow.com/questions/3792066/convert-utc-dates-to-local-time-in-php
#
function format_timestamp($timestamp_in, $default_timezone='America/New_York'): string
{
    if ($timestamp_in) {
        try {
            $dt_out_utc = new DateTimeImmutable($timestamp_in, new DateTimeZone('UTC'));
            $dt_out_eastern = $dt_out_utc->setTimezone(new DateTimeZone($default_timezone));
            $result = $dt_out_eastern->format('Y-m-d H:i:s T');
        } catch (DateMalformedStringException $e) {
            error_log("Invalid timestamp in $timestamp_in: " . $e->getMessage());
            $result = "";
        } catch (DateInvalidTimeZoneException $e) {
            error_log("Invalid time zone in $timestamp_in: " . $e->getMessage());
            $result = "";
        }
    } else {
        $result = "";
    }
    return $result;
}