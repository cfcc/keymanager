<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/7/16
 * Time: 12:18 PM
 */
$page_title = "Admin:Home";
$include_path = "../";
$logout_url = "../index.php";
$js_include = array('../resources/js/sha512.js', '../resources/js/forms.js');

require_once "../include/functions.php";

sec_session_start();

require_once "../include/header.php";

if (isset($_GET['error'])) {
    echo '<div class="alert alert-danger" role="alert">' . "\n";
    echo '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>' . "\n";
    echo '<span class="sr-only">Error:</span>' . "\n";
    echo 'Invalid username or password' . "\n";
    echo '</div>' . "\n";
}

if (login_check_ldap(ACCESS_USER)) {
    echo "<p>You are logged in</p>";
    echo "<p><a href='../include/logout_actions_ajax.php' class='btn btn-default'>Log Out</a></p>";
} else {
    echo "<p>You must be logged in to access these pages.</p>\n";
}
?>
    <hr>
    <div class="row footer-content">
        <a href="register_station.php" class="btn btn-default">Register a Station</a>
    </div>

<?php
require_once("../include/footer.php");
