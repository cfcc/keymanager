<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/6/16
 * Time: 1:17 PM
 */
require_once "include/comparableinterval.php";
require_once 'include/app_config.php';
require_once 'include/database.php';
require_once 'include/functions.php';

sec_session_start();

try {
    $config = load_config();
    $db = connect_key_db($config);
} catch (Exception $ex) {
    error_log($ex);
    $fatal_error = $ex->getMessage();
}

$date_error_message = null;

if (!empty($_REQUEST['all'])) {
    $link = "<a href='report.php' class='btn btn-default'>Show This Week</a>";
    $limit = new DateTime('1970-1-1');
} else {
    if (!empty($_REQUEST['asof'])) {
        $link = "<a href='report.php?all=true' class='btn btn-default'>Show All</a>";
        try {
            $limit = new DateTime($_REQUEST['asof']);
        } catch (Exception $e) {
            $date_error_message = "Invalid Date. " . $e->getMessage();
            $limit = new DateTime('now');
        }
    } else {
        $quarter_date = new DateTime('-3 months');
        $link = "<a href='report.php?asof=" . $quarter_date->format('Y-m-d') . "' class='btn btn-default'>Show This Quarter</a>";
        $limit = new DateTime('now');
        $limit->modify('last week');
    }
}

$max_hours_out = getSetting('MAX_HOURS_OUT', $db, '0');

if (login_check_ldap()) {
    $allowed_depts = $_SESSION['user_info']['groups'];
} else {
    $station_id = checkStation($db);
    $allowed_depts = checkDepts($station_id, $db);
}

$questionmarks = str_repeat("?,", count($allowed_depts)-1) . "?";

$query = "SELECT checkout_log.keyring_id, out_timestamp, S_OUT.name AS out_station_name, in_timestamp, S_IN.name AS in_station_name, colleague_id, first_name, last_name
 FROM checkout_log 
 LEFT JOIN staff ON checkout_log.staff_id = staff.id_barcode 
 LEFT JOIN stations S_OUT ON S_OUT.id=checkout_log.out_station 
 LEFT JOIN stations S_IN ON S_IN.id=checkout_log.in_station
 LEFT JOIN keyring_depts ON keyring_depts.keyring_id=checkout_log.keyring_id
 WHERE staff_id=staff.id_barcode
 AND out_timestamp>?
 AND keyring_depts.depts_id IN ($questionmarks)
 GROUP BY checkout_log.keyring_id,out_timestamp
 ORDER BY out_timestamp DESC";

$stmt = $db->prepare($query);

$page_title = 'Activity Report';

require_once("include/header.php");
if ($date_error_message) {
    echo "<div class='alert alert-danger'>$date_error_message</div>";
}
?>
    <div class="row text-right">
        <p>Report as of: <?= $limit->format("Y-m-d") ?>, <?= $link ?></p>
    </div>
    <table class="table" id="activity">
        <thead>
        <tr>
            <th>Key Ring</th>
            <th>Name</th>
            <th>Check-Out Time</th>
            <th>Station</th>
            <th>Check-In Time</th>
            <th>Station</th>
            <th>Time Out</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $args = array_merge(array($limit->format('Y-m-d')), $allowed_depts);
        $stmt->execute($args);
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $name = $row['first_name'] . ' ' . $row['last_name'];
            $result = checkTimeOut($row['out_timestamp'], $row['in_timestamp'], $max_hours_out);
            if ($result['diff'] == -1) {
                echo "<tr>\n";
            } else {
                if ($row['in_timestamp']) {
                    echo "<tr class='bg-warning'>\n";
                } else {
                    echo "<tr class='bg-danger'>\n";
                }
            }
            echo "<td>" . $row['keyring_id'] . "</td>";
            echo "<td>" . $name . "</td>";
            echo "<td>" . format_timestamp($row['out_timestamp']) . "</td>";
            echo "<td>" . $row['out_station_name'] . "</td>";
            echo "<td>" . format_timestamp($row['in_timestamp']) . "</td>";
            echo "<td>" . $row['in_station_name'] . "</td>";
            if ($result['hours']->days > 0) {
                echo "<td>" . $result['hours']->format('%a day(s)') . "</td>\n";
            } else {
                echo "<td>" . $result['hours']->format('%H:%I:%S') . "</td>\n";
            }
            echo "</tr>\n";
        }
        ?>
        </tbody>
    </table>
<?php
    require_once("include/footer.php");
