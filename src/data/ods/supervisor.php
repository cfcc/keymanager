<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 9/27/16
 * Time: 9:26 AM
 */
header("content-type:application/json");

include_once '../../include/app_config.php';
include_once '../../include/database.php';
include_once '../../include/functions.php';

$config = load_config();
$ods_db = connect_ods_db($config);

$json_out = array(
    'supervisor_email' => null
);

if (isset($_REQUEST['id'])) {
    $supervisor_email = getSupervisorEmail($ods_db, $_REQUEST['id']);
    $json_out['supervisor_email'] = $supervisor_email;
} else {
    header('HTTP/1.1 500 ' . "Missing required parameter: id");
}

echo json_encode($json_out);

exit();
