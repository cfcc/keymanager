<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/14/16
 * Time: 2:23 PM
 */
$page_title = 'Add Me';

require_once "include/app_config.php";
include_once 'include/database.php';
include_once 'include/functions.php';

sec_session_start();

include_once 'include/header.php';

try {
    $config = load_config();
    $db = connect_key_db($config);
    $ods_db = connect_ods_db($config);
} catch (Exception $ex) {
    $db = null;
    $ods_db = null;
    error_log($ex);
    $fatal_error = $ex->getMessage();
}

if (!isset($_POST['colleague_id'])) {
    ?>
    <div class="row">
        <form action="addme.php" method="post">
            <div class="form-group small-form-left">
                <label for="colleague_id">Enter Your Colleague ID:</label>
                <input type="text" maxlength="7" id="colleague_id" name="colleague_id" class="form-control" autofocus required>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <hr>
    <div class="row footer-content">
        <a href="admin/register_station.php" class="btn btn-default">Register a Station</a>
    </div>
    <?php
} else {
    $colleague_id = $_POST['colleague_id'];
    if (!isset($_POST['id_barcode'])) {
        // First check that the ID is in ODS as an employee
        $stmt = $ods_db->prepare("SELECT HRPER_ID, LAST_NAME, FIRST_NAME FROM ODS_HRPER
LEFT JOIN SPT_PERSTAT ON HRP_LATEST_PERSTAT_ID=SPT_PERSTAT.PERSTAT_ID
LEFT JOIN SPT_PERSON_NON_CORP ON HRPER_ID=ID
WHERE SPT_PERSTAT.PERSTAT_END_DATE IS NULL
AND HRPER_ID=?");
        $stmt->execute(array($colleague_id));
        if ($stmt->columnCount() > 0) {
            $result = $stmt->fetchAll();
        } else {
            $result = NULL;
        }
        if ($result) {
            // Check for an old BARCODE in the system, so we can disable it
            $full_name = $result[0]['FIRST_NAME'] . " " . $result[0]['LAST_NAME'];
            $stmt2 = $db->prepare("SELECT id_barcode FROM staff WHERE colleague_id=? AND is_active=TRUE");
            $stmt2->execute(array($colleague_id));
            if ($stmt2->columnCount() > 0) {
                $result2 = $stmt2->fetchAll();
            } else {
                $result2 = NULL;
            }
            if ($result2) {
                $id_barcode = $result2[0]['id_barcode'];
            } else {
                $id_barcode = "";
            }
            ?>
            <div class="row">
                <form action="addme.php" method="post">
                    <input type="hidden" id="old_id_barcode" name="old_id_barcode" value="<?php echo $id_barcode; ?>">
                    <div class="form-group small-form-left">
                        <label for="colleague_id">Enter Your Colleague ID:</label>
                        <input type="text" maxlength="7" id="colleague_id" name="colleague_id"
                               class="form-control" readonly value="<?php echo $colleague_id; ?>">
                        <span class="help-block"><?php echo $full_name; ?></span>
                    </div>
                    <div class="form-group small-form-left">
                        <label for="id_barcode">Scan your barcode:</label>
                        <input type="text" id="id_barcode" name="id_barcode" class="form-control" autofocus required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <?php
        } else {
            ?>
            <div class='alert alert-danger'>
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error: </span>
                The ID you entered, <?= $_POST['colleague_id'] ?>, is not in the database.
            </div>
            <p><a href="addme.php" class="btn btn-default">Star Over</a></p>
            <?php
        }
    } else {
        $id_barcode = $_POST['id_barcode'];
        $old_id_barcode = $_POST['old_id_barcode'];

        $records_changed = 0;
        $error_msg = "";

        if ($id_barcode != $old_id_barcode) {
            $stmt = $ods_db->prepare("SELECT ID, FIRST_NAME, LAST_NAME FROM SPT_PERSON_NON_CORP WHERE ID = ?");
            $stmt->execute(array($colleague_id));
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $name = $result[0]['FIRST_NAME'] . ' ' . $result[0]['LAST_NAME'];
            try {
                // These steps are reversed because there is a constraint on the database that the ID barcode must be
                // unique, and we don't want to deactivate the current one until we verify the new one is valid.
                $stmt2 = $db->prepare("INSERT INTO staff (id_barcode, colleague_id, is_active, last_name, first_name) VALUES (?, ?, ?, ?, ?)");
                $stmt2->execute(array($id_barcode, $colleague_id, True, $result[0]['LAST_NAME'], $result[0]['FIRST_NAME']));
                $records_changed += $stmt2->rowCount();

                if ($old_id_barcode != "") {
                    $stmt = $db->prepare("UPDATE staff SET is_active=FALSE WHERE id_barcode=?");
                    $stmt->execute(array($old_id_barcode));
                    $records_changed += $stmt->rowCount();
                }
            } catch (PDOException $e) {
                $error_msg = 'Database exception: ' . $e->getMessage();
            }

            if ($records_changed > 0) {
                ?>
                <div class="row">
                    <div class="alert alert-success">
                        Thank you, <?= $name; ?>, your record has been updated.
                    </div>
                    <p>Colleague ID: <?php echo $colleague_id; ?></p>
                    <p>ID Barcode: <?php echo $id_barcode; ?></p>
                    <p><a href="index.php" class="btn btn-default">Finish</a></p>
                </div>
                <?php
            } else {
                ?>
                <div class='alert alert-danger'>
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error: </span>
                    Unable to update the database at this time.
                </div>
                <?php if (!empty($error_msg)) { echo "<pre>" . $error_msg . "</pre>\n"; } ?>
                <p><a href="addme.php" class="btn btn-default">Star Over</a></p>
                <?php
            }
        } else {
            ?>
            <div class='alert alert-warning'>
                <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
                <span class="sr-only">Warning: </span>
                The barcode, <?= $id_barcode ?>, is already associated with your Colleague ID, <?= $colleague_id ?>
            </div>
            <p><a href="addme.php" class="btn btn-default">Star Over</a></p>
            <?php
        }
    }
}

include_once 'include/footer.php';
