#!/bin/bash
docker-compose up -d keymanager-db
sleep 5
docker cp database.sql keymanager_db:/database.sql
read -s -p "MariaDB Password:" MARIADB_PASSWORD
echo ""
docker exec -i keymanager_db sh -c "exec mariadb -uroot -p'$MARIADB_PASSWORD' < /database.sql"
read -s -p "Application Password:" APP_PASSWORD
echo ""

# FIXME: this is failing
echo "CREATE USER 'keymgr'@'%' IDENTIFIED BY '$APP_PASSWORD';" |
docker exec -i keymanager_db sh -c "exec mariadb -uroot -p'$MARIADB_PASSWORD'"

echo "GRANT ALL PRIVILEGES ON \`keymanager\`.* TO 'keymgr'@'%';" | 
docker exec -i keymanager_db sh -c "exec mariadb -uroot -p'$MARIADB_PASSWORD'"

CREATE USER 'sammy'@'localhost' IDENTIFIED BY 'password';