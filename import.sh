#!/bin/bash
docker-compose up -d keymanager-db
sleep 5
docker cp keymanager/keymanager.sql keymanager_db:/database.sql
read -s -p "MariaDB Password:" MARIADB_PASSWORD
echo ""
docker exec -i keymanager_db sh -c "exec mariadb -uroot -p'$MARIADB_PASSWORD' < /database.sql"
