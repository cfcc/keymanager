<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/7/16
 * Time: 9:15 AM
 */
$page_title = "Admin:Keyring";
$include_path = "../";
$logout_url = "../index.php";
$js_include = array(
    'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'
);
$page_css = array(
    'https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
);
$page_script = array(
    "$(document).ready(function() {",
    "  $('#keyring_list').DataTable( { searching: false } );",
    "} );"
);
require_once "../include/app_config.php";
require_once '../include/database.php';
require_once "../include/functions.php";

$config = load_config();
$db = connect_key_db($config);
$ods_db = connect_ods_db($config);

sec_session_start();

require_once "../include/header.php";

if (login_check($config, $db, ACCESS_MANAGER)) {

    if (array_key_exists('key_barcode', $_REQUEST)) {
        $access_allowed = false;
        $stmt = $db->prepare("SELECT * FROM keyring WHERE key_barcode=?");
        $stmt->execute(array($_REQUEST['key_barcode']));
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            $record = $result[0];
            $my_depts = getDeptsForKeyring($_REQUEST['key_barcode'], $db, $ods_db);
            if ($_SESSION['access'] == ACCESS_ADMINISTRATOR) {
                # by default Administrators have access to everything
                $access_allowed = true;
            } else {
                foreach ($_SESSION['user_info']['groups'] as $this_dept) {
                    if (in_array($this_dept, array_keys($my_depts))) {
                        $access_allowed = true;
                        break;
                    }
                }
            }
            $select_default_dept = false;
        } else {
            # anyone can add a key ring
            $access_allowed = true;
            $record = array(
                'key_barcode' => '',
                'description' => ''
            );
            $my_depts = array();
            $select_default_dept = true;
        }
        if ($access_allowed) {
            if ($_REQUEST['action'] == 'edit') {
                ?>
                <div class="row">
                    <div class="col-md-6 col-md-offset-1">
                <form action="keyring.php" autocomplete="off" method="post">
                    <input type="hidden" name="action" value="save">
                    <input type="hidden" name="current_key_barcode" value="<?php echo $record['key_barcode'] ?>">
                    <div class="form-group">
                        <label for="key_barcode">Key Barcode</label>
                        <input type="text" id="key_barcode" name="key_barcode" class="form-control"
                               value="<?php echo $record['key_barcode'] ?>" maxlength="12" autofocus>
                    </div>
                    <div class="form-group">
                        <label for="key_description">Description</label>
                        <input type="text" id="key_description" name="key_description" class="form-control"
                               value="<?php echo $record['description'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="add_dept">Departments</label>
                        <ul>
                        <?php
                        foreach ($my_depts as $dept_code => $dept_name) {
                            echo "<div class='checkbox'>\n<li><strong>" . $dept_name . "</strong>";
                            echo " ( <label>\n<input type='checkbox' name='del_dept[" . $dept_code . "]'> Delete\n</label> )\n";
                            echo "</li></div>\n";
                        }
                        ?>
                        </ul>
                    </div>
                    <div class="form-group">
                        <select id="add_depts" name="add_depts[]" size="10" multiple>
                            <option value="0">Select a department to add...</option>
                            <?php
                            foreach (getAllDepts($ods_db, $db) as $dept_code => $dept_name) {
                                echo "<option value='" . $dept_code . "'";
                                if ($select_default_dept) {
                                    if (in_array($dept_code, $_SESSION['user_info']['groups'])) {
                                        echo " selected";
                                    }
                                }
                                echo ">" . $dept_name . "</option>\n";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="keyring.php" class="btn btn-default">Cancel</a>
                    </div>
                </form>
                    </div>

                </div>
                <?php
            } elseif ($_REQUEST['action'] == 'delete') {
                echo "<p>Delete is not supported yet</p>\n";
                echo "<div class='btn-group'>";
                echo "<a href='keyring.php' class='btn btn-default'>Cancel</a>";
                echo "</div>\n";
            } elseif ($_REQUEST['action'] == 'save') {
                $records_changed = 0;
                if (!empty($_REQUEST['current_key_barcode'])) {
                    $stmt = $db->prepare("UPDATE keyring SET key_barcode=?, description=? WHERE key_barcode=?");
                    $stmt->execute(array($_REQUEST['key_barcode'], $_REQUEST['key_description'], $_REQUEST['current_key_barcode']));
                    $records_changed = $stmt->rowCount();
                    if ($_REQUEST['key_barcode'] != $_REQUEST['current_key_barcode']) {
                        // Find the current records
                        $stmt = $db->prepare("SELECT * FROM keyring_depts WHERE keyring_id=?");
                        $stmt->execute(array($_REQUEST['current_key_barcode']));
                        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        // remove the current records
                        $stmt = $db->prepare("DELETE FROM keyring_depts WHERE keyring_id=?");
                        $stmt->execute(array($_REQUEST['current_key_barcode']));
                        $records_changed += $stmt->rowCount();
                        // create new records
                        $stmt = $db->prepare("INSERT INTO keyring_depts (keyring_id, depts_id) VALUES (?, ?)");
                        foreach ($result as $rec) {
                            echo "<p>[DEBUG] " . $_REQUEST['key_barcode'] . ", " . $rec['depts_id'] . "</p>\n";
                            $stmt->execute(array($_REQUEST['key_barcode'], $rec['depts_id']));
                            $records_changed += $stmt->rowCount();
                        }
                    }
                } else {
                    $stmt = $db->prepare("INSERT INTO keyring (key_barcode, description) VALUES (?, ?)");
                    $stmt->execute(array($_REQUEST['key_barcode'], $_REQUEST['key_description']));
                    $records_changed = $stmt->rowCount();
                }
                if (isset($_REQUEST['del_dept'])) {
                    $records_removed = 0;
                    $stmt = $db->prepare("DELETE FROM keyring_depts WHERE keyring_id=? AND depts_id=?");
                    foreach ($_REQUEST['del_dept'] as $key => $value) {
                        $stmt->execute(array($_REQUEST['current_key_barcode'], $key));
                        $records_removed += $stmt->rowCount();
                    }
                    echo "<p>Removed " . $records_removed . " record(s)</p>";
                }
                if (isset($_REQUEST['add_depts'])) {
                    $stmt = $db->prepare("INSERT INTO keyring_depts (keyring_id, depts_id) VALUES (?, ?)");
                    foreach ($_REQUEST['add_depts'] as $dept_code) {
                        $stmt->execute(array($_REQUEST['key_barcode'], $dept_code));
                        $records_changed += $stmt->rowCount();
                    }
                }
                echo "<p>Updated records: " . $records_changed . "</p>\n";
                echo "<p><a href='keyring.php' class='btn btn-default'>Continue</a></p>\n";
            }
        } else {
            echo "<h2>Access Denied</h2>\n";
            echo "<p>You don't have access to modify that keyring.</p>\n";
            echo ">Back</a></p>\n";
        }
    } else {
        ?>
        <div class="col-md-2 col-md-offset-10">
            <p><a href="keyring.php?action=edit&key_barcode=" class="btn btn-default">Add</a></p>
        </div>
        <table id="keyring_list" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Key Barcode</th>
                <th>Description</th>
                <th>Department</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($_SESSION['access'] == ACCESS_ADMINISTRATOR) {
                $stmt = $db->query("SELECT key_barcode, description, GROUP_CONCAT(depts_id ORDER BY depts_id ASC SEPARATOR ',') AS dept
                                              FROM keyring
                                              LEFT JOIN keyring_depts ON keyring_id=key_barcode
                                              GROUP BY key_barcode");
            } else {
                $allowed_depts = $_SESSION['user_info']['groups'];
                $questionmarks = str_repeat("?,", count($allowed_depts)-1) . "?";
                $stmt = $db->prepare("SELECT K0.key_barcode, K0.description, (SELECT group_concat(depts_id SEPARATOR ',') FROM keyring_depts WHERE keyring_id=K0.key_barcode) AS dept
                                                FROM keyring K0
                                                  LEFT JOIN keyring_depts KD1 ON K0.key_barcode=KD1.keyring_id
                                                WHERE KD1.depts_id IN ($questionmarks);");
                $stmt->execute($allowed_depts);
            }
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                ?>
                <tr>
                    <td><?php echo $row['key_barcode']; ?></td>
                    <td><?php echo $row['description']; ?></td>
                    <td><?php echo $row['dept']; ?></td>
                    <td>
                        <div class="btn-group">
                            <a href="keyring.php?action=edit&key_barcode=<?php echo $row['key_barcode'] ?>" class='btn btn-default'>Edit</a>
                            <a href="keyring.php?action=delete&key_barcode=<?php echo $row['key_barcode'] ?>" class='btn btn-default'>Delete</a>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <?php
    }
} else {
    echo "<p>You are not authorized to access this page.  Please login first.</p>";
}
require_once "../include/footer.php";
