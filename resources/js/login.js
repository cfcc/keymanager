/**
 * Created by jfriant80 on 8/22/16.
 */
function authenticate(next_url, base_url) {
    var username = $('#username').val();
    var password = $('#password').val();
    if (username == "") {
        alert("Username cannot be empty");
        $('#username').focus();
        return;
    }
    if (password == "") {
        alert("Password cannot be empty");
        $('#password').focus();
        return;
    }
    if (!base_url) { base_url = ""}
    $.ajax({
        type:'POST',
        dataType: 'text',
        url: base_url + 'include/login_actions_ajax.php',
        data:{
            username:username,
            password:password
        },
        success:function(data,textStatus){
            if (is_valid_domain(next_url)) {
                window.location=next_url;
            } else {
                window.location.reload(true);
            }
        },
        error: function (xhr,textStatus, errorThrown){
            alert(errorThrown);
        }
    });
}

function can_submit(this_event, base_url) {
    // if the user has pressed enter in this textbox, then we'll try to run the authenticate function
    if (this_event.which == 13 || this_event.keyCode == 13) {
        authenticate('', base_url);
        return false;
    }
    return true;
}

function is_valid_domain(url) {
    var patterns = {};
    var success = false;

    patterns.protocol = '^^(http(s)?(:\/\/))?(www\.)?';
    patterns.domain = '([a-zA-Z0-9-_\.]+)';

    var url_regex = new RegExp(patterns.protocol + patterns.domain, 'gi');
    var match = url_regex.exec(url);
    if (match) {
        console.log('[DEBUG] ' + match[5] + " == " + window.location.hostname);
        if (match[5] === window.location.hostname){
            // FQDN is good if it matches the server
            success = true;
        } else if (match[5] === '..' || match[5] === '.') {
            // Relative path OK too
            success = true;
        }
    }
    return success;
}

function do_logout(include_path, next_url) {
    $.ajax({
        type:'POST',
        dataType: 'text',
        url: include_path + 'include/logout_actions_ajax.php',
        data:{},
        success:function(data,textStatus){
            if (is_valid_domain(next_url)) {
                window.location=next_url;
            } else {
                window.location.reload(true);
            }
        },
        error: function (xhr,textStatus, errorThrown){
            alert(errorThrown);
        }
    });
}