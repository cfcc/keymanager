<?php

const DEFAULT_INI_LOCATION = '/home/keymanager/settings.ini';

/**
 * @throws Exception if the config file is missing
 */
function load_config(?string $config_filename=NULL): array
{
    # Define all the settings with blank or default values
    $config = array(
        'database' => array(
            'host' => '',
            'dbname' => 'keymanager',
            'user' => '',
            'password' => '',
            'charset' => 'utf8mb4'
        ),
        'ods' => array(
            'host' => '',
            'dbname' => '',
            'username' => '',
            'password' => '',
            'charset' => 'utf8mb4'
        ),
        'ad' => array(
            'hostname' => '',
            'user_dn' => '',
            'domain' => '',
            'user_groups' => array(),
            'manager_groups' => array(),
            'administrator_groups' => array()
        ),
        'mail' => array(
            'host' => '',
            'username' => '',
            'password' => '',
            'port' => '',
            'supervisor_cc' => false
        )
    );
    if ($config_filename == NULL) {
        $config_filename = DEFAULT_INI_LOCATION;
    }
    # Try to load the settings from a file and then insert them in the to the config array
    if (file_exists($config_filename)) {
        $result = parse_ini_file($config_filename, true);
        foreach ($result as $key => $value) {
            $config[$key] = $value;
        }
    } else {
        throw new Exception("Unable to load configuration file '$config_filename'.");
    }
    return $config;
}

function save_config(string $settings_content, ?string $config_filename=NULL): bool
{
    if ($config_filename == NULL) {
        $config_filename = DEFAULT_INI_LOCATION;
    }
    $success = FALSE;
    try {
        $fp = fopen($config_filename, 'w');
        if ($fp) {
            fwrite($fp, $settings_content);
            fclose($fp);
            $success = TRUE;
        }
    } catch (Exception $e) {
        echo "<p>Configuration saving failed</br>ERROR: " . $e->getMessage() . "</p>";
    }
    return $success;
}
