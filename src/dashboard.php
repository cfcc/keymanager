<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/6/16
 * Time: 2:59 PM
 */
require_once 'include/app_config.php';
require_once "include/database.php";
require_once "include/functions.php";

try {
    $config = load_config();
    $db = connect_key_db($config);
} catch (Exception $ex) {
    $db = null;
    error_log($ex);
    $fatal_error = $ex->getMessage();
}

sec_session_start();

$refresh_rate = getSetting('REFRESH_SECONDS', $db, '5');

if (login_check_ldap()) {
    $allowed_depts = $_SESSION['user_info']['groups'];
} else {
    $station_id = checkStation($db);
    $allowed_depts = checkDepts($station_id, $db);
}
$questionmarks = str_repeat("?,", count($allowed_depts)-1) . "?";

$stmt = $db->prepare("SELECT key_barcode, description, (SELECT id FROM checkout_log WHERE in_timestamp is NULL AND checkout_log.keyring_id=keyring.key_barcode) AS borrower
FROM keyring
LEFT JOIN keyring_depts ON keyring_id=key_barcode
WHERE depts_id IN ($questionmarks)
GROUP BY key_barcode");

$stmt2 = $db->prepare("SELECT out_timestamp, stations.name AS out_station_name, colleague_id, first_name, last_name
FROM checkout_log
LEFT JOIN staff ON checkout_log.staff_id = staff.id_barcode
LEFT JOIN stations ON out_station=stations.id
WHERE checkout_log.id=?");

// $stmt3 = $ods_db->prepare("SELECT ID, FIRST_NAME, LAST_NAME FROM ODS_Person WHERE ID = ?");

$page_title = "Dashboard";
$page_meta = array('<meta http-equiv="refresh" content="' . $refresh_rate . '" >');

require_once("include/header.php");
?>
    <table class="table" id="dboard">
        <thead>
        <tr><th>Key Ring</th><th>Name</th><th>Check-Out Time</th></tr>
        </thead>
        <tbody>
        <?php
        $stmt->execute($allowed_depts);
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            echo "<tr><td>" . $row['description'] . "</td>";
            if ($row['borrower']) {
                $stmt2->execute(array($row['borrower']));
                $result = $stmt2->fetch(PDO::FETCH_ASSOC);
                $name = $result['first_name'] . ' ' . $result['last_name'];
                echo "<td>" . $name . "</td>";
                echo "<td>" . format_timestamp($result['out_timestamp']);
                if ($result['out_station_name']) {
                    echo " <small>(" . $result['out_station_name'] . ")</small>";
                }
                echo "</td>";
            } else {
                echo "<td class='text-success'>Checked In</td><td></td>";
            }
            echo "</tr>\n";
        }
        ?>
        </tbody>
    </table>
<script type="application/javascript">
    let table = new DataTable('#dboard', {
        "paging": false,
        "info": false
    });
</script>


<?php
require_once("include/footer.php");
?>