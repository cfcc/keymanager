<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/7/16
 * Time: 9:16 AM
 */
if (empty($title)) { $title = "Key Manager"; }
if (empty($include_path)) { $include_path = ""; }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>
        <?php
        echo $title;
        if (! empty($page_title)) { echo " - " . $page_title . "\n"; }
        ?>
    </title>
    <?php
    if (!empty($page_meta) && is_array($page_meta)) {
        foreach ($page_meta as $line) {
            echo $line . "\n";
        }
    }
    ?>

    <!-- Bootstrap -->
    <?php
        echo '<link href="' . $include_path . 'resources/css/bootstrap.min.css" rel="stylesheet" media="screen">' . "\n";
        echo '<link href="' . $include_path . 'resources/css/site.css" rel="stylesheet">' . "\n";

        if (!empty($page_css) && is_array($page_css)) {
            foreach ($page_css as $line) {
                echo '<link href="' . $line . '" rel="stylesheet">' . "\n";
            }
        }
    ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <!-- Datatables JS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/2.0.8/css/dataTables.dataTables.css" />

    <script type="text/javascript" src="https://cdn.datatables.net/2.0.8/js/dataTables.js"></script>

    <?php
    echo "<script src=\"" . $include_path . "resources/js/login.js\"></script>\n";
    if (isset($js_include)) {
        foreach ($js_include as $url) {
            echo "    <script src='" . $url . "'></script>\n";
        }
    }
    if (!empty($page_script) && is_array($page_script)) {
        echo "\n<script type='JavaScript'>\n";
        foreach ($page_script as $line) {
            echo $line . "\n";
        }
        echo "</script>\n";
    }
    ?>
</head>
<body>

<?php
    try {
        require_once("navbar.php");
    } catch (Exception $e) {
        error_log($e->getMessage());
    }
?>

<div class="container">
    <?php
    if (!empty($page_title)) {
        echo "<div class='page-header'><h1>" . $page_title . "</h1></div>\n";
    }
    if (isset($_SESSION['flash'])) {
        echo "<div class='row'>\n";
        echo "<div class='alert alert-warning' role='alert'>\n";
        echo "<span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\"></span>\n";
        echo "<span class=\"sr-only\">Warning:</span>\n";
        echo $_SESSION['flash'] . "\n";
        echo "</div>\n";
        echo "</div>\n";
        unset($_SESSION['flash']);
    }
    if (isset($fatal_error)) {
        echo "<div class='row'>\n";
        echo "<div class='alert alert-danger' role='alert'>\n";
        echo "<span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\"></span>\n";
        echo "<span class=\"sr-only\">Error:</span>\n";
        echo "<b>Fatal Error:</b> " . $fatal_error . "\n";
        echo "</div>\n";
        echo "</div>\n";
        unset($fatal_error);
    }
    ?>
