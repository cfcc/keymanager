<?php
$page_title = "Key Check-In/Check-Out";

require_once "include/app_config.php";
require_once "include/database.php";
require_once "include/functions.php";

$menu = array(
    'Home' => 'index.php',
    'Activity Report' => 'report.php',
    'Dashboard' => 'dashboard.php',
    'Add Me' => 'addme.php',
);

sec_session_start();

try {
    $config = load_config();
    $db = connect_key_db($config);

    $station_id = checkStation($db);
    if (!is_null($station_id)) {
        $hide_logon_form = true;
    }
} catch (Exception $ex) {
    $fatal_error = $ex->getMessage();
}

include_once 'include/header.php';

?>
    <div class="row">
        <form action="start.php" method="post">
            <div class="form-group small-form-left">
                <label for="key_barcode" class="lead">Key Barcode</label>
                <input type="text" class="form-control" id="key_barcode" name="key_barcode" autofocus autocomplete="off"
                       required>
                <p class="help-block lead">Scan the key ring barcode to get started</p>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
<?php
require_once("include/footer.php");
