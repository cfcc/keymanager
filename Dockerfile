FROM php:8-apache-bookworm

RUN \
apt-get update \
    && apt-get install -y --no-install-recommends \
    libldap2-dev \
    unixodbc-dev \
    gnupg \
    libssh2-1 \
    libssh2-1-dev \
    libicu-dev \
    wget \
    zlib1g-dev \
    libpng-dev \
    cron \
    && apt-get update \
    && rm -rf /var/lib/apt/lists/* \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
    && docker-php-ext-install gd ldap pdo pdo_mysql mysqli\
    && pecl install sqlsrv \
    && pecl install pdo_sqlsrv \
    && docker-php-ext-enable gd mysqli \
    && docker-php-ext-enable sqlsrv pdo_sqlsrv pdo pdo_mysql \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

# ------------ Install MS SQL client deps ------------ #
# adding custom MS repository
RUN curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor -o /usr/share/keyrings/microsoft-prod.gpg \
    && curl https://packages.microsoft.com/config/debian/12/prod.list > /etc/apt/sources.list.d/mssql-release.list

# install SQL Server drivers and tools
RUN apt-get update && ACCEPT_EULA=Y apt-get install -y msodbcsql18 mssql-tools18 \
    && echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc \
    && /bin/bash -c "source ~/.bashrc"

RUN apt-get -y install locales \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen

# These files are now being created automatically as part of the installation steps above
# RUN echo "extension=sqlsrv.so" >> /usr/local/etc/php/conf.d/docker-php-ext-sqlsrv.ini \
#     && echo "extension=pdo_sqlsrv.so" >> /usr/local/etc/php/conf.d/docker-php-ext-pdo-sqlsrv.ini
# -------------- END MSSQL -------------------------------- #

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

# Add Apache site configuration and other scripts
COPY etc/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY etc/start-apache.sh /usr/local/bin/start-apache.sh
COPY etc/notify.sh /usr/local/bin/notify.sh

# Copy application source
COPY src /var/www/html
COPY resources /var/www/html/resources
# Copy required packages supplied through Composer
COPY vendor /var/www/html/vendor

COPY etc/crontab /etc/cron.d/cron

# Set up the crontab and add cron to the startup script
# See https://tecadmin.net/running-a-cronjob-inside-docker/
RUN chmod 0644 /etc/cron.d/cron \
    && crontab /etc/cron.d/cron \
    && sed -i 's/^exec /service cron start\n\nexec /' /usr/local/bin/apache2-foreground

RUN chown -R www-data:www-data /var/www \
    && chmod 755 /usr/local/bin/start-apache.sh \
    && chmod 755 /usr/local/bin/notify.sh

CMD ["start-apache.sh"]
