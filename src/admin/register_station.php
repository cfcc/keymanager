<?php
/**
 * Created by PhpStorm.
 * User: jfriant80
 * Date: 7/27/16
 * Time: 3:01 PM
 */

$page_title = "Admin:Station";
$include_path = "../";
$logout_url = "../index.php";
$js_include = array(
    'https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js'
);
$page_css = array(
    'https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css'
);

require_once '../include/database.php';
require_once "../include/functions.php";

sec_session_start();

require_once "../include/header.php";

$config = load_config();
$db = connect_key_db($config);

$stmt_find = $db->prepare("SELECT id FROM stations WHERE uid=?");
$stmt_add = $db->prepare("INSERT INTO stations (uid, ip_address, name, is_active) VALUES (?, ?, 'Temporary', FALSE)");

if (array_key_exists('HTTP_X_REAL_IP', $_SERVER)) {
    $actual_ip = $_SERVER['HTTP_X_REAL_IP'];
} else {
    $actual_ip = $_SERVER['REMOTE_ADDR'];
}

$ip_bin = inet_pton($actual_ip);
$uid = md5($actual_ip);

echo "<div class='row'>\n";

$stmt_find->execute(array($uid));
$result = $stmt_find->fetchAll(PDO::FETCH_ASSOC);

if (!$result) {
    try {
        $stmt_add->execute(array($uid, $ip_bin));
        echo "<div class='alert alert-success' role='alert'>Registered this station as " . $uid . "</div>\n";
    } catch (PDOException $e) {
        echo "<div class='alert alert-danger' role='alert'>Error: " . $e->getMessage() . "</div>\n";
    }
} else {
    echo "<div class='alert alert-info' role='alert'>Station ID has already been registered as " . $uid . "</div>\n";
}
echo "</div>\n";

require_once("../include/footer.php");
